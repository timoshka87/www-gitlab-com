require 'extensions/breadcrumbs'
require 'lib/homepage'
require 'extensions/partial_build_blog'

# Per-page layout changes:
#
# With no layout

# Disable HAML warnings
# https://github.com/middleman/middleman/issues/2087#issuecomment-307502952
Haml::TempleEngine.disable_option_validator!

page '/*.xml', layout: false
page '/*.json', layout: false
page '/*.txt', layout: false

set :haml, {
  format: :xhtml
}

activate :syntax, line_numbers: false

set :markdown_engine, :kramdown
set :markdown, tables: true, hard_wrap: false, input: 'GFM'

activate :blog do |blog|
  blog.name = 'blog'
  # This will add a prefix to all links, template references and source paths
  blog.prefix = 'blog'
  blog.sources = 'blog-posts/{year}-{month}-{day}-{title}.html'
  blog.permalink = '/{year}/{month}/{day}/{title}/index.html'
  blog.layout = 'post'
  # Allow draft posts to appear on all branches except master (for Review Apps)
  blog.publish_future_dated = true if ENV['CI_COMMIT_REF_NAME'].to_s != 'master'

  blog.summary_separator = /<!--\s*more\s*-->/

  blog.custom_collections = {
    categories: {
      link: '/categories/{categories}/index.html',
      template: '/category.html'
    }
  }
  # blog.tag_template = '/templates/tag.html'
  # blog.taglink = '/blog/tags/{tag}/index.html'
end

activate :blog do |blog|
  blog.name = 'releases'
  # This will add a prefix to all links, template references and source paths
  blog.prefix = 'releases'
  blog.sources = 'posts/{year}-{month}-{day}-{title}.html'
  blog.permalink = '/{year}/{month}/{day}/{title}/index.html'
  blog.layout = 'post'
  # Allow draft posts to appear on all branches except master (for Review Apps)
  blog.publish_future_dated = true if ENV['CI_BUILD_REF_NAME'].to_s != 'master'

  blog.summary_separator = /<!--\s*more\s*-->/

  blog.custom_collections = {
    categories: {
      link: '/categories/{categories}/index.html',
      template: '/category.html'
    }
  }
  # blog.tag_template = '/templates/tag.html'
  # blog.taglink = '/blog/tags/{tag}/index.html'
end

activate :autoprefixer do |config|
  config.browsers = ['last 2 versions', 'Explorer >= 9']
end

activate :breadcrumbs, wrapper: :li, separator: '', hide_home: true, convert_last: false
activate :partial_build_blog

# Reload the browser automatically whenever files change
unless ENV['ENABLE_LIVERELOAD'] != '1'
  configure :development do
    activate :livereload
  end
end

# Build-specific configuration
configure :build do
  set :build_dir, '../../public'
  activate :minify_css
  activate :minify_javascript
  activate :minify_html, preserve_line_breaks: true
end

# Don't include the following into the sitemap
ignore '/templates/*'
ignore '/includes/*'
ignore '/source/stylesheets/highlight.css'
ignore '/category.html'
ignore '/.gitattributes'
ignore '**/.gitkeep'
