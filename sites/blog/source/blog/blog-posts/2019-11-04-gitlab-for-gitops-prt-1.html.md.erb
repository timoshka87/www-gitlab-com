---
title: "Why collaboration technology is critical for GitOps"
author: Sara Kassabian
author_gitlab: skassabian
author_twitter: sarakassabian
categories: company
image_title: '/images/blogimages/gitopsseries.png'
description: "How GitLab can be the single source of truth for infrastructure and deployment teams."
tags: git, inside GitLab
ee_cta: false
install_cta: false
twitter_text: "How GitLab empowers the pillars of GitOps, collaboration, process, and version control."
featured: yes
postType: content marketing
merch_banner: merch_five
merch_sidebar: merch_five
---

_While there are plenty of DevOps tools that can fulfill some of the functions of GitOps, GitLab is the only tool that can take your application from idea to code to deployment all in one collaborative platform. GitLab strategic account leader Brad Downey shows users how we make GitOps work in a three-part blog and video series. In part one, we dig deeper into GitOps in terms of process and explain how GitLab was designed to fulfill these functions._

“GitOps” is the latest buzzword in the DevOps lexicon, and as with any new concept it has [many different interpretations](https://devops.com/an-inside-look-at-gitops/). At its core though, [GitOps](/topics/gitops/) refers to using a Git repository as the single source of truth for all the code that goes into building infrastructure and deploying applications.

Some level of automation is required to deploy the code to various clouds, and there are declarative tools to help expedite this process. For example, Terraform can be used to provision Kubernetes to any publicly available cloud, while Helm and Kubernetes can be used to deploy applications to those Kubernetes clusters.

# GitOps and GitLab

By using a version control system such as Git as the single source of truth, engineers are able to update the underlying source code for their applications in a continuous delivery format.

“The version control system ensures everything is recorded and visible and an audit trail keeps teams compliant,” says [Brad Downey](https://about.gitlab.com/company/team/#bdowney), strategic account leader at GitLab, in an [article for DevOps.com](https://devops.com/an-inside-look-at-gitops/). “GitOps will make it easy to revert problematic changes, becoming a single source of truth about what is happening in the system from both the software development and infrastructure perspective.”

GitLab is a single application for the entire DevOps lifecycle that is built on Git. In addition to being a vital tool for application development, GitLab is a collaboration platform that allows any and all stakeholders to weigh in on the code production process.

“Collaboration is key to this entire [GitOps] process,” says Brad. “Infrastructure teams, development teams, even management, project management, security, and business stakeholders, all need to collaborate together to produce this code in a fast and efficient manner.”

In the first video in our series, Brad shares an example of how GitLab has allowed teams to perform GitOps well before the process had its name.

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube-nocookie.com/embed/wk7YAXijIZI" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

# Using GitLab

## Planning a project with epics

<%= partial "includes/blog/blog-merch-sidebar" %>

Brad created an example epic called [Scale the Cloud](https://gitlab.com/groups/gitops-demo/infra/-/epics/2) to demonstrate the process behind scaling up a Kubernetes cluster in GitLab for this GitOps series.

Since GitOps is deployment centered on version control, the first step is to define the scope of the project and identify the stakeholders. Next, we share any other information that might be necessary to make the project happen, e.g., the coding, changes to infrastructure as code, what changes must be reviewed and eventually deployed to production.

After opening an [epic](https://docs.gitlab.com/ee/user/group/epics/) in the associated repository, you can add some of the goals and tasks in the description. An epic allows you to track issues across different projects and milestones. An [issue](https://docs.gitlab.com/ee/user/project/issues/#issues) is the main medium for collaborating ideas and planning work in GitLab. Because GitLab is multi-cloud, Brad opens three separate issues for the demo that articulate what is required to deploy the Kubernetes cluster to each unique environment: [Azure (AKS)](https://gitlab.com/gitops-demo/infra/azure/issues/1), [Google (GKE)](https://gitlab.com/gitops-demo/infra/gcp/issues/4), and [Amazon (EKS)](https://gitlab.com/gitops-demo/infra/aws/issues/3).

## Fostering collaboration and transparency with GitLab

We can see at the epic level that the issue for [scaling inside the EKS cluster](https://gitlab.com/gitops-demo/infra/aws/issues/3) has already been completed. Clicking the [issue](https://gitlab.com/gitops-demo/infra/aws/issues/3) reveals that a merge request was created from the tasks outlined in the issue, and that the MR is already merged.

To see what exactly has changed between the original code and current code, click inside the MR.

“I can see all the tests that passed before I merged it, and after,” says Brad. “I could also see what was changed in the comment history. And, making a note that I approved and merged it myself.”

The issue for scaling to GKE is not yet completed. When Brad opens the merge request, we see that it is still a [Work in Progress (WIP)](https://docs.gitlab.com/ee/user/project/merge_requests/work_in_progress_merge_requests.html#work-in-progress-merge-requests), meaning nothing has been changed yet. There is a comment on the MR from Terraform, which shows that the node count needs to change from two nodes to five nodes to prepare the GKE environment for deployment. Since Brad is also the approver for this MR, he clicks Resolve the WIP Status to kick off the pipeline, and opts to delete the source branch to merge the updated node count.

“In terms of GitOps, it isn't just about the code, it's about the collaboration. And GitLab enables the collaboration, everybody to be working on the same page,” says Brad.

In order for GitLab to be an effective collaboration tool, it also needs to be transparent which is why everyone in the organization is able to see an issue and associated MR by default. The issue and MR can be assigned to a collaborator, or the collaborator can be tagged in the comments section to have it added to their [To Do list](https://docs.gitlab.com/ee/user/todos.html).

Navigating back to the Epic view, which is what stakeholders will often use to view project progress, we see that the deployment for scaling GKE to five nodes is underway.

“Everybody is able to work from the same system and understand where things are at,” says Brad. “Whether you're in infrastructure or whether you're in application development, all changes follow the same process of, defining the body of work, assigning it to individuals, collaborating with teammates, and then deploying that code and using the Git repository as that single source of truth.”

_Read [part two](/blog/2019/11/12/gitops-part-2/) of our GitOps series to see how infrastructure teams can use GitLab and Terraform to build dynamic infrastructure for applications._

<%= partial "includes/blog/blog-merch-banner" %>
