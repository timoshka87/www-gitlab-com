---
layout: handbook-page-toc
title: "Facebook response workflow"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Overview

In Zendesk, each advocate [should have Facebook set up as a personal view](/handbook/marketing/community-relations/community-advocacy/tools/zendesk/#view-limits-workaround). This view monitors direct messages sent to the GitLab Facebook page and posts on the GitLab Facebook wall.

The expert involvement workflow can not currently be used in this workflow, as responses must come from the GitLab Facebook page. However, expert input is often necessary to respond to user's questions here.

## Workflow

1. Open each ticket in the private Facebook Zendesk view.
1. If the message is spam or does not require a response, simply mark the ticket as `mention` and close the ticket.
1. Direct message and wall post tickets that require a response can be responded to direcly from Zendesk. There are no tags or macros that need to be applied to Facebook tickets.
1. If the message is a support related question, consider posting the content in #support_gitlab-com or #support_self-managed on slack. You can also point users to post their question on the [GitLab Forum](https://forum.gitlab.com/)
1. Many questions asked via Facebook are hiring related. Below are some links that can be shared with applicants as they navigate the hiring and application process.

*  [GitLab Jobs Page](https://about.gitlab.com/jobs/), for the most current postings
*  [Jobs FAQ Page](https://about.gitlab.com/jobs/faq/), for general questions and learning

Other more specific hiring questions can be asked in #people-ops or #recruiting in slack.

## Best practices

*  Advocates should always remember to check the Facebook view daily. Since this is a private view and not part of the main views in Zendesk it can be easy to forget.

## Automation

Messages sent to our [Facebook page](https://www.facebook.com/gitlab/) feed into ZenDesk.
