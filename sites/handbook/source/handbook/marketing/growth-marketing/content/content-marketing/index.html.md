---
layout: markdown_page
title: "Content Marketing"
---

## On this page
{:.no_toc}

- TOC
{:toc}

The Content Marketing team is responsible for strategic content creation. The goal
of the Content Marketing team is to build an engaged audience of people who exhibit
specific, desirable behaviors (e.g. willingness to share personal data) through
repeatable and scalable content programs.

The Content Marketing team's focus is on creating end-to-end content experiences
that bring continued and consistent value to our audience. We value quality over
quantity and seek to deliver content that is backed by research, is useful, and actionable.

## Content Pillar Strategy

We use the content pillar strategy to organize and plan our work. Pillars are definied within epics on a quarterly basis and work is broken out into milestones and issues. When planning a quarterly pillar, use the following process: 

1. **Align to relevant events.** Identify which corporate events are coming up in the following quarter that are related to your area of expertise. What is the common theme across the events? Use this to determine the angle of your pillar. Contact the event team to determine what, if any, content they need to support the event. 
1. **Align with the campaigns team.**  

### What is a content pillar?

A content pillar is a go to market content strategy that is aligned to a high-level theme (for example, Just Commit) and executed via content sets. Often, content pillars are defined based on [value drivers and customer use cases](/handbook/marketing/#Go_to_market:_value_drivers and_customer_use_cases)in order to support go to market strategy. For example, "Just commit to application modernization" is a content pillar about improving application infrastructure in order to deploy faster. Within this pillar, many topics can be explored (CI/CD, cloud native, DevOps automation, etc.) and the story can be adapted to target different personas or verticals.  

We use content pillars to plan our work so we can provide great digital experiences to our audiences. The content team aligns to themes to ensure we are executing strategically and meeting business goals. Pillars allow us to narrow in on a specific topic and audience, and sets help us break our work into more manageable components. Each set created should produce an end-to-end content experience (awareness to decision) for our audience.

![Content pillar diagram](/images/handbook/marketing/corporate-marketing/content-pillar.png)

#### What's included in a content set?

Here's an example of what's included in a content set:

| Quantity | Stage | Content Type | DRI |
| ------ | ------ | ------ | ------ |
| 4 | Awareness | Thought leadership blog post | Content marketing |
| 1 | Awareness | Topic webpage | Content marketing |
| 1 | Awareness | Resource | Content marketing |
| 4 | Consideration| Technical blog post | Content marketing |
| 1 | Consideration | Whitepaper | Product & technical marketing |
| 1 | Consideration | Solution page | Content & product marketing |
| 2 | Consideration | Webcast | Product & technical marketing |
| 1 | Purchase | Demo | Technical marketing |
| 1 | Purchase | Data sheet | Product marketing |


**Sources:**

- [What Is a Content Pillar? The Foundation for Efficient Content Marketing](https://kapost.com/b/content-pillar/)
- [How to Create Pillar Content Google Will Love](https://contentmarketinginstitute.com/2018/04/pillar-content-google/)
- [What Is a Pillar Page? (And Why It Matters For Your SEO Strategy)](https://blog.hubspot.com/marketing/what-is-a-pillar-page)
- [Content for Each Buying Stage of the Consumer Purchase Cycle](https://contentwriters.com/blog/content-consumer-purchase-cycle/)

## Planning timeline

Pillar strategy is planned annually and reviewed quarterly. Content sets are planned quarterly and reviewed monthly. When planning, we follow the 80/20 rule: 80% of capacity is planned leaving 20% unplanned to react to last minute opportunities.

- **2 weeks prior to start of the quarter:** Proposed content plans are published to the content schedule. Product marketing, digital marketing, and corporate events marketing give feedback on the plan.
- **1 week prior to the start of the quarter:** Kickoff calls are held.
- **1st day of the quarter:** Content plans are finalized.
- **1st of each month:** Progress reviews are held. Plans are adjusted if needed.
- **Last day of the quarter:** Cross-functional retrospective is held.

## Gated asset creation workflow 
Gated assets are created and written by the content team. The process will vary, however the content strategy and timeline should be consistent. Several other teams are involved in the planning process for whitepapers and eBooks. To learn more about the campaign and marketing strategy, the [Marketing Programs Management section](/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/gated-content/#overview)) of the handbook has an overview.

All gated assets should have an epic. There are many teams involved in creating the final product and an epic helps keep al realted issues together. The epic is the single source of truth for workflow deadlines. Gated assets include eBooks, whitepapers, demos, and webcasts. All gated assets can be found on the [Resources](/resources/) page.

Visit the [Gated Content page](/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/gated-content/#gated-content) for detailed instructions on [how to structure your epic, issues, and timeline.](/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/gated-content/#timeline-and-process-alignment) 

### Types of content marketing assets

#### Whitepaper

A whitepaper is a technical and focused topic study intended to educate a prospective buyer during the Consideration or Purchase stages of a campaign. The whitepaper offers a problem and solution instance in a granular, technical tone. The content team member should collaborate closely with their product marketing counterpart when researching and writing the asset so that the content reaches appropriate technical standards for the intended audience. 

**Examples:** 
1. [A seismic shift in application security](/resources/whitepaper-seismic-shift-application-security/)
2. [How to deploy on AWS from GitLab](/resources/whitepaper-deploy-aws-gitlab/)

#### eBook

An eBook tends to be broader in scope than a whitepaper and provides a clear definition of the topic, along with various industry standard best practices. eBooks typically provide awareness-level content, but can dive deeper into GitLab if it's intended for late-stage consumption. The content team member can usually take the lead on developing eBook content, with input and review from their product marketing counterpart. More technical or instructive eBooks might require more collaboration with product marketing.

**Examples:**
1. [The GitLab Remote Playbook](/resources/ebook-remote-playbook/)
2. [The benefits of single application CI/CD](/resources/ebook-single-app-cicd/)

**Timeline:** 1-3 weeks to plan, research, and draft an eBook. Use [the content resource request template](https://gitlab.com/gitlab-com/marketing/growth-marketing/global-content/content-marketing/-/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=#) for planning with team members and to define a specific deadline. All eBooks should have a launch date on the [editorial calendar](https://about.gitlab.com/handbook/marketing/growth-marketing/content/editorial-team/#blog-calendar).

**Deliverables:**
* Week 1: Share a detailed outline with involved team members.
* Week 2: Share a first draft with the team and include review parameters and deadlines.
* Week 3: Reviews are completed. You may need an additional week and second round of review if significant content changes are made.

#### Infographic

An infographic is an illustrated overview of a topic or process, and is typically an ungated asset. Infographics should tell a story using data, diagrams, and text. It can be used to discuss industry trends, relate insights, or explain different stages of a project. When planning an infographic, it's important to meet with the design DRI before writing the content for the asset. Together, figure out a structure for the graphic, and ask the design DRI for recommended copy lengths.

### Epic template

```
**Epic title:** [asset type] Working Title - Launch date

## Launch date: TBD

* Content DRI:   
* MPM DRI: 

### Deadlines

* [ ]  2020-XX-XX Open copy issue and complete asset brief (-27 business days from launch)
* [ ]  2020-XX-XX Landing page copy due (-27 business days from launch)
* [ ]  2020-XX-XX Final copy due  (-15 business days from launch)
* [ ]  2020-XX-XX Final design due (-5 business days from launch)
* [ ]  2020-XX-XX MR ready to launch (-3 business days from launch)
* [ ]  2020-XX-XX Asset added to pathfactory (-3 business days from launch)
```


