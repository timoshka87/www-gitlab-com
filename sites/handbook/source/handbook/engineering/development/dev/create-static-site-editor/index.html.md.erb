---
layout: handbook-page-toc
title: Static Site Editor Team
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Vision

The Static Site Editor Team is part of the [Dev](/handbook/engineering/development/dev/) Section and is responsible for enhancing the editing experience for static sites inside of GitLab.

## Team Members

The following people are permanent members of the Handbook Team:

<%= direct_team(role_regexp: /Create:Static Site Editor/, manager_role: 'Backend Engineering Manager, Create:Static Site Editor') %>

## Stable Counterparts

The following members of other functional teams are our stable counterparts:

<%= stable_counterparts(role_regexp: /[,&] (Create(?!:)|Create:Static Site Editor)/, direct_manager_role: 'Backend Engineering Manager, Create:Static Site Editor') %>

## How to reach us

Depending on the context here are the most appropriate ways to reach out to the Static Site Editor group:
- GitLab epics/issue/MRs: `@gl-static-site-editor`
- Email: `static-site-editor@gitlab.com`
- Slack: `#g_create_static_site_editor` and `@static-site-editor-team`

## Projects

The team works primarily on the two projects listed below:

### GitLab Product
The work in [GitLab](https://gitlab.com/gitlab-org/gitlab/) revolves around enhancing the editing experience for static sites.
See the [Static Site Editor](https://about.gitlab.com/direction/create/static_site_editor/) strategy for more information.

### GitLab Handbook
The work in the [GitLab Handbook](https://about.gitlab.com/handbook/) site is primarily focused on:
1. Reducing the time it takes from merge to deployment
1. Enhancing the editing, reading and sharing (think presenting a handbook page) functionality
1. Maintaining the integrity of the handbook site

See the [GitLab Handbook](https://about.gitlab.com/direction/create/gitlab_handbook/) strategy for more information.
Have a look at this epic to track the improvement efforts in the handbook: https://gitlab.com/groups/gitlab-com/-/epics/423

#### about.gitlab.com Responsibility
The handbook is currently part of the larger [about.gitlab.com](https://gitlab.com/gitlab-com/www-gitlab-com/) website repository which in addition to the handbook includes the marketing website, blog, jobs etc.

The repository is undergoing a refactor into a monorepo structure and the handbook will be split out into its own project. Progress can be followed here: [https://gitlab.com/groups/gitlab-com/-/epics/282](https://gitlab.com/groups/gitlab-com/-/epics/282)

### GitLab Docs

The Static Site Editor engineering team develops and maintains the [GitLab Docs project](https://gitlab.com/gitlab-org/gitlab-docs).

Work is tracked on our Development Workflow board which can be viewed here: https://gitlab.com/gitlab-org/gitlab-docs/-/boards/1668857

#### Process

1. The Technical Writers, are responsible for creating and prioritizing issues for the improvement of the GitLab Docs project.
1. Once an issue has been verified and fleshed out by the TW team, it is moved into planning on the engineering side by assigning the `workflow::planning breakdown` label to it.
1. The EM is responsible for reviewing issues labelled `workflow::planning breakdown` and if determined that it is ready for implementation assign the `workflow::ready for development` label to it.
1. The EM schedules issues that are ready for development by adding a milestone, assignee and the `Deliverable` label to it.
1. Team members that have completed their `Deliverables` for a milestone are free to choose issues from the backlog (i.e. in `workflow::ready for development`) to work on. Issues will always be listed in priority order from top to bottom on the [workflow board](https://gitlab.com/gitlab-org/gitlab-docs/-/boards/1668857).

## Meetings

The Static Site Editor group has two weekly group calls ([agenda](https://docs.google.com/document/d/1Hc1veBD1EfItCp8qCz3n4v6ttS47GphzubLTqkNCVqE/edit#)):
1. Mondays, 15:00 UTC - Engineering focused
1. Mondays, 23:00 UTC - Product & Design focused

These two calls allow the majority of the team members to attend the relevant discipline focused call. Attendance is optional, but we encourage all team members to catch up on the recording of both meetings each week.

We also have a bi-weekly retro call on a Friday at 13:00 UTC.

### Ad-hoc sync calls

We operate using async communication by default. There are times when a sync discussion can be beneficial and we encourage team members to schedule sync calls with the required team members as needed.

When scheduling a sync call do the following:
1. Add it to the team's shared calendar and make sure to attach an agenda doc
1. Notify the team of the call in the [#g_create_static_site_editor_eng](https://gitlab.slack.com/archives/CU90JTCF8) Slack channel.
1. Record the call and upload the recording to the [Static Site Editor](https://www.youtube.com/playlist?list=PL05JrBw4t0KorLPV59dxkMkKo5Hjc7tyE) playlist on the GitLab Unfiltered YouTube channel.
1. Notify the team of the new available recording in the [#g_create_static_site_editor_eng](https://gitlab.slack.com/archives/CU90JTCF8) Slack channel.

### Shared Calendar

The group has a shared calendar called `Static Site Editor Group` (gitlab.com_56i46dodsa0mvtkfvn10hcssjo@group.calendar.google.com) where all group relevant meetings are shared on as well as team member PTO _(refer to [PTO Ninja](#pto-ninja) below)_.

## GitLab Unfiltered Playlist

The Static Site Editor group collates all video recordings related to the group and it's team members in the [Static Site Editor playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0KorLPV59dxkMkKo5Hjc7tyE) in the [GitLab Unfiltered](https://www.youtube.com/channel/UCMtZ0sc1HHNtGGWZFDRTh5A) YouTube channel.

<iframe width="800" height="450" src="https://www.youtube.com/embed/videoseries?list=PL05JrBw4t0KorLPV59dxkMkKo5Hjc7tyE" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Work

In general, we use the standard GitLab [engineering workflow](/handbook/engineering/workflow/). To get in touch
with the Create:Static Site Editor team, it's best to create an issue in the relevant project
(typically [GitLab](https://gitlab.com/gitlab-org/gitlab/issues) or [www-gitlab-com](https://gitlab.com/gitlab-com/www-gitlab-com/issues)) and add the `~"group::static site editor"` labels, along with any other
appropriate labels. Then, feel free to ping the relevant Product Manager and/or
Engineering Manager as listed above.

For more urgent items, feel free to use [#g_create_static_site_editor](https://gitlab.slack.com/archives/g_create_static_site_editor) on Slack.

### Epic/Issue process

In the Static Site Editor group we use epics to plan out our category maturity roadmap and the user stories in it.

The Static Site Editor category strategy epic can be viewed here: <https://gitlab.com/groups/gitlab-org/-/epics/2688>

The following tree structure indicates the epic and issue hierarchy we follow for the feature development of our maturity roadmap:

```
└── category strategy epic
    └── maturity level epic
        └── user story epic
            └── feature issue
                └── implementation issue
                    └── implementation MR
```

- Each maturity level (minimal, viable, complete, lovable) is represented by an epic which holds further epics describing the user stories.
- A user story epic then consists out of one or more feature issues that detail the requirements of the features that make up the user story.
- Feature issues are then split out further into implementation issues.
- Each implementation issue is then delivered through one or more MRs.

#### Guidelines

- We collaborate at the user story epic level using comments to discuss the user story requirements and what features it might involve.
- The PM is the DRI for user story epics and any changes to the description of the user journey requires their approval.
- We try to scope feature issues so that they can be delivered in a single release.
- We collaborate at the feature issue level using comments to discuss the feature requirements and consider implementation considerations.
- We do not require a 1:1 mapping between MRs and implementation issues, instead we encourage engineers to iterate to the implementation issue requirements through multiple small MRs
- A feature issue might require multiple team members to implement it. For instance it could require work across the design, backend and frontend disciplines, however, it will only have one DRI.
- We only assign one DRI to an implementation issue

#### Parent/Child issues

We use the concept of parent/child issues to track a feature (parent) issue's progress independently from the implementation (child) issues.
During the feature issue planning process the DRI will identify the various implementation issues that will be needed to deliver on the requirements.

These implementation issues will be created and linked to the feature issue as a blocker for the feature issue.
This allows multiple team members to work on a feature issue, with each one assigned as the DRI for their own implementation issue.

Implementation issues progress through the workflow stages independently from the feature issue.

#### Issue/Epic creation responsibility

| Topic                | Responsible                  |
|----------------------|------------------------------|
| Maturity level epic  | Product Manager              |
| User story epic      | Product Manager              |
| Feature issue        | Product Manager              |
| Implementation issue | Feature issue DRI            |

- Aim to keep the *Feature issue* titles descriptive but succinct
    - Ex. `Remove YAML front matter from editable content in the Static Site Editor`
- Prefix *Implementation issue* titles with the succinct *Feature issue* title follow by a `::` syntax. Feel free to abbreviate "Static Site Editor" with SSE in the implementation issue.
    - Ex. `Remove YAML front matter from editable content in the SSE::Sync model in component`

#### User story planning process

The engineering team undertakes technical planning for each user story epic.
Due to our iteration value we keep our planning process light, with the main aim of providing clarity around requirements and a high-level technical solution.

The expectation of the planning process is that by the end of it we:
1. Agree on the overall user story epic scope
1. Agree on the high-level technical approach
1. Agree on the requirements, scope and tasks of the feature issues making up the user story.
1. Confirm which disciplines (design, frontend, backend) will need to work on the feature issues and whether it is suitable for multiple engineers to be assigned to it
1. Ensure that each feature issue is the smallest possible iteration from a functional point of view

##### The process
1. Requirements analysis
    1. The **Product Manager** alerts the group when a user story epic and its feature issues are ready for review.
    1. The team then reviews the requirements asynchronously to get an initial understanding of what will need to be implemented and to identify any gaps in the detail provided.
    1. The **Product Manager** incorporates the feedback by updating the user story epic description and requesting updates to any design mockups from the **Product Designer**.
1. Technical planning
    1. Once the user story epic, feature issues have been updated the **Engineering Manager** will assign a **Planning DRI** to analyze the requirements and create an initial technical solution.
    1. The **Planning DRI** can be any engineer from the team.
    1. The **Planning DRI** will make a copy of the [Static Site Editor - Engineering Planning - Template](https://docs.google.com/document/d/1KS9qgK6jh_WwwPVXDYM9l9JQRn7W2PKz7GKQQQ14t7I/edit), and add their planning notes in it. This will be used as the basis of joint technical planning session.
    1. Once the initial planning is completed the **Planning DRI** schedules a synchronous call for the engineering team members to discuss the requirements and technical solution in a planning session.
    1. Out of this planning session might come actions for further research to be done and additional synchronous calls might be scheduled or the planning may continue asynchronously.
    1. It is the responsibility of the **Planning DRI** to ensure that the planning is completed.
    1. The final action of the **Planning DRI** is to add new or update existing feature issues with all relevant information and to add the implementation tasks.
        1. An implementation task should ideally represent some user-facing value which is being delivered, and can be verified in the UI.
        1. An implementation task should be specific to a single discipline (design, backend, frontend)

##### Planning cadence

The creation of user story epics, its validation and the creation of design mockups is an ongoing process that is not tied to a specific milestone. There could be many epics being worked on at any single point in time.

To encourage focus and not to overwhelm the engineering team with multiple priorities, planning on user story epics will focus on one epic at a time, in priority order as determined by the **Product Manager**.

#### Feature issue planning process

During milestone planning the PM and EM will identify which features we are targeting to deliver during a specific milestone.
Once finalized the EM will assign the feature issues to an engineer to drive implementation.

Once assigned the engineer becomes the DRI for planning and coordinating the implementation of the feature issue.

##### The process
1. EM assigns feature issue to an engineer who becomes the DRI
1. DRI takes initial user story planning information (if exists) and does necessary further planning to come up with the best solution for executing the requirements.
1. DRI creates the implementation issues and where needed consults with the EM on who can assist with the implementation issues if it requires other team members.
1. DRI oversees the implementation of the feature and updates the feature issue to indicate the correct workflow state.
1. DRI is responsible for final validation of the feature issue before closing it.

##### Planning cadence

Feature issue planning can happen at any point in time, however, ideally all feature issue planning should be completed in the week before the milestone starts.

#### Issues Status updates

The status of implementation issues is reviewed in every group call.

Engineers are responsible for updating the status of all issues labeled with the `Deliverable` label weekly on a Monday, before the group sync call.
If the status is anything other than `On track` it should be accompanied by a comment in the issue that provides context on the status.

##### Status definitions

To be consistent with our status definitions the following guideline should be used when setting the status.

- `On track` = on track to make it into milestone, meaning it will hit the gitlab.com production site before or on the 17th of the month. Ideally all deliverables should be merged by 16th. If MRs for backfilling tests or documentation won't make it into the milestone create a separate issue for it.
- `Needs attention` = where either requirements clarification, additional engineering support or EM/PM intervention is needed to get it back on track to make it into the milestone.
- `At risk` = where an issue is unlikely to make the milestone even if it receives attention.

### Capacity planning

The Static Site Editor group does **NOT** make use of weights to rigidly plan capacity or track velocity.
Rather a continuous delivery mentality is adopted, facilitated through using a Kanban-style work management approach to moving work through the product workflow process.

### What to work on

The primary source for things to work on are the development workflow boards:
- Handbook Category: [Development Workflow Board](https://gitlab.com/gitlab-com/www-gitlab-com/-/boards/1625854?label_name[]=group%3A%3Astatic%20site%20editor)
- Static Site Editor Category: [Development Workflow Board](https://gitlab.com/gitlab-org/gitlab/-/boards/1624109?label_name[]=group%3A%3Astatic%20site%20editor)

#### What to work on first

Deliverables are considered top priority and are expected to be done by the end
of the iteration cycle in time for the release on the 22nd. To make it into the release in time
it has to be merged and deployed to production by the 17th of the month.

These top priority issues are assigned to engineers by the Engineering Manager before the milestone begins.

Many things can happen that can result in a deliverable not actually being completed by the end of a milestone,
and while this usually indicates that the Engineering Manager was too optimistic in their estimation
of the issue's complexity, or that an engineer's other responsibilities ended up
taking up more time than expected, this should never come as a surprise to the
Engineering Manager.

The sooner this potential outcome is anticipated and communicated, the more time
there is to see if anything can be done to prevent it, like reducing the scope
of the deliverable, or finding a different engineer who may have more time to
finish a deliverable that hasn't been started yet.

If this outcome cannot be averted and the deliverable ends up missing the
milestone, it will simply be moved to the next milestone to be finished up, and the
engineer and Engineering Manager will have a chance to
[retrospect](#retrospectives) and learn from what happened.

Generally, your deliverables are expected to take up about 75% of the
time you spend working in a month. The other 25% is set aside for other
responsibilities (code review, community merge request coaching, [helping
people out in Slack, participating in discussions in issues](/handbook/values/#collaboration),
etc), as well as urgent issues that come up during the month and need someone
working on them immediately (regressions, security issues, customer issues, etc).

#### What to work on next

If you have time to spare after finishing your deliverables and other
activities, you can pick any issue that is ready for development
(i.e. is in the `workflow::ready for development` column).

The issues in this column are ordered by priority so choosing from the top of the list is recommended.

If anything is blocking you from getting started with the top issue immediately,
like unanswered questions or unclear requirements, you can skip it and consider
a lower priority issue, as long as you put your findings and questions in the
issue, so that the next engineer who comes around may find it in a better state.

Instead of picking up an issue from the Handbook or Static Site Editor boards,
you may also choose to spend any spare time working on anything else that you believe
will have a significant positive impact on the product or the company in general.
As the [general guidelines](/handbook/marketing/community-relations/community-advocacy/guidelines/general/) state, "we recognize that inspiration is
perishable, so if you’re enthusiastic about something that generates great
results in relatively little time feel free to work on that."

We expect people to be [managers of one](/handbook/values/#efficiency) and prefer responsibility
over rigidity, so there's no need to ask for permission if you
decide to work on something that's not on the issue board, but please keep your
other responsibilities in mind, and make sure that there is an issue, you are
assigned to it, and consider sharing it in [#g_create_static_site_editor](https://gitlab.slack.com/archives/g_create_static_site_editor).

### Workflow labels

The easiest way for engineering managers, product managers, and other stakeholders
to get a high-level overview of the status of all issues in the current milestone,
or all issues assigned to specific person, is through the Development Workflow boards,
which has columns for each of the workflow labels described on Engineering Workflow
handbook page under [Updating Issues Throughout Development](/handbook/engineering/workflow/#updating-issues-throughout-development).

As owners of the issues assigned to them, engineers are expected to keep the
workflow labels on their issues up to date, either by manually assigning the new
label, or by dragging the issue from one column on the board to the next.

### Issue labels

When creating an issue that relates to our group make sure to always add the following labels:
- Our group label: `group::static site editor`
- Category label:
    - `Category:Static Site Editor` for issues relating to the product we are building
    - `Category:GitLab Handbook` for issues relating to work we are doing on the GitLab Handbook in [gitlab-com/www-gitlab-com](https://gitlab.com/gitlab-com/www-gitlab-com)
- A discipline label (`frontend` and/or `backend`) if relevant to indicate which disciplines will need to be involved in actioning the issue.

The EM and PM is responsible for setting any other relevant labels and milestones on the issues.

### MR labels

All MRs¹ should contain the following labels to accurately reflect in our [metrics dashboard](https://app.periscopedata.com/app/gitlab/574876/Static-Site-Editor-Team-Metrics-Dashboard):
- Our group label: `group::static site editor`
- One of the following throughput categorization labels:
    - `feature` - when introducting new or updated functionality
    - `bug` - when fixing a bug
    - `backstage` - when doing behind the scenes work.
    - `security` - when fixing a security vulnerability

¹ Exception for `gitlab-com/www-gitlab-com` repo: Content related MRs (think fixing a spelling mistake, updating the info on the team page etc) should not contain the group label as it should not count towards our throughput.

### Team collaboration guidelines

These guidelines extend the [team collaboration guidelines](https://about.gitlab.com/handbook/engineering/workflow/#working-in-teams)
documented in the Engineering workflow page.

#### Frontend and backend collaboration

Some deliverables have frontend and backend components. For those cases, the backend and frontend
engineers should coordinate in advance the data contract expectations between both parts of the
stack and document those expectations in the deliverable’s issue. These are examples of data
contracts that team members can agree in advance:

- Which endpoints are required.
- API request data structure.
- API response data structure.
- Error handling.

This initial plan does not replace communication during the development process.
The engineers involved in the development of a feature should continuously communicate the status
of their work.

#### Requesting feedback

You should strive for receiving feedback early and often in your Merge Requests.
If your Merge Request delivers frontend work that depends on unfulfilled backend
requirements, demo your work using mock data based on the data contract expectations
defined at the beginning of the development process.

For example, you can prepopulate a VueX store or a VueApollo cache with initial data to demo
a feature that introduces read-only enhancements. You can also simulate HTTP responses in your
Vuex actions to demo a workflow that mutates data.

If you are working on a backend Merge Request, it is a good practice to request feedback from your
Frontend engineer counterpart. After all, the Frontend engineer will be the main user of your work.

#### Feature flags, changelog entries, documentation

If a deliverable has backend and frontend components, develop those components behind a
[feature flag](https://docs.gitlab.com/ee/development/feature_flags/#feature-flags-in-development-of-gitlab).
Once all the components are merged into master, create a Merge Request that:

- Integrates the backend and frontend components.
- Adds documentation for the deliverable if necessary.
- Adds a changelog entry.

Do not add a changelog entry in your Merge Requests if your work is hidden behind a feature flag.

#### Using workflow labels

We use workflow labels to identify the current stage of a deliverable. Use the following guidelines
to assign a workflow label:

- Assign the `~workflow::in dev` once you have started to work on a deliverable.
- Assign the `~workflow::in review` once all the tasks required to complete a deliverable are under review.
- Assign the `~workflow::verification` when the deliverable is demonstrable on production.

#### Using issue tasks

When multiple contributors are assigned to an issue:
- Ensure a task list exists in the description
- Each contributor's `@handle` is appended to their task item(s) to convey explicit ownership
    - This lends itself to GitLab's use of a [*directly responsible individual* (DRI)](https://about.gitlab.com/handbook/people-group/directly-responsible-individuals/#what-is-a-directly-responsible-individual), but at the task vs. project level
- When a merge request is made for a task, append the MR link to the task
- When a merge request is successfully merged, check the task's checkbox to communicate that the task is 100% complete

When a single contributor is assigned to an issue all tasks are implied to be owned by the issue owner.

### Code Reviews

Inline with [GitLab's code review guidelines](https://docs.gitlab.com/ee/development/code_review.html) we default to assigning the first review to a team member.
This not only ensures that team members are up-to-date with changes to the product, but facilitates a faster review turn-around.

When assigning an MR to a maintainer, default to a team member (if there is someone other than yourself), to ensure fastest possible turn-around.

While GitLab's current [Review-response SLO](https://docs.gitlab.com/ee/development/code_review.html#review-response-slo) is 2 business days, we aim for 1 business day for internal team reviews.

For trivial MRs feel free to assign to any GitLab team member, defaulting to the recommendation from Reviewer Roulette.

### Async standup

<%= partial("handbook/engineering/development/dev/create/async_standup.erb") %>

### Retrospectives

We use [issues](https://gitlab.com/gl-retrospectives/create-stage/static-site-editor/-/issues) to facilitate an async retrospective process for each milestone.

Process:
1. A private issue is [opened automatically on the 27th of the month](https://gitlab.com/gitlab-org/async-retrospectives)
1. We capture feedback in the issue throughout the milestone and have bi-weekly sync calls to discuss the feedback (see [meetings](#meetings)).
1. The feedback is captured against the following categories:
    1. What went well during the last two weeks?
    1. What didn't go well during the last two weeks?
    1. What can we improve on?
1. During the sync call team members verbalize their feedback and we discuss it.
1. We add a comment to track any actions that we identify during the discussions.
1. Each action is assigned to a specific team member and it should be considered high priority to action it.
1. Due to the fact that we encourage a safe, non-judgemental environment for team members to openly discuss challenges and vent any frustration we do not make the recording of the retro public.
1. Recordings are uploaded to the group's Google Drive folder here: <https://drive.google.com/drive/folders/1QdT3lVI2BEVU4zSsmC4vrKZmbo7LMN1Q> (access restricted to group team members only)
1. Once the milestone is completed and we have reviewed all feedback we review and sanitize and written down feedback before making the issue visible to GitLab team members.

## Career development

<%= partial("handbook/engineering/development/dev/create/career_development.erb", locals: { group: "Static Site Editor" }) %>

## Onboarding

New team members to the Static Site Editor group can use the resources below, in addition to their general onboarding, to get up to speed.

| Resource | Comments  |
|---|---|
| [Product Section Direction - Dev](https://about.gitlab.com/direction/dev/) | Read the Product Direction for the Dev section to get a better understanding of the area of the product that we fall into |
| [Category Direction - Static Site Editor](https://about.gitlab.com/direction/create/static_site_editor/) | Read the Static Site Editor category direction to understand more about the product we are developing |
| [Static Site Editor Group Call Agenda](https://docs.google.com/document/d/1Hc1veBD1EfItCp8qCz3n4v6ttS47GphzubLTqkNCVqE/edit)  | Read the notes from our past group calls and watch the recordings to get an idea of what we discuss in our weekly sync call. Recording links are provided below each date heading and you can view the full list of recordings [here in Google Drive](https://drive.google.com/drive/search?q=static-site-editor-weekly%20mp4) |
| [🧠 Think Big : Static Site Editor](https://docs.google.com/document/d/1yruS9EY0TVMpdiaqz4E6a_D0g7vRGT53qO-7ir9u4OQ/edit#heading=h.25ukr6qi85ry) | We hold Think Big sessions to explore where we might want to take our category in the future. These sessions help inform what our vision looks like and the roadmap we take to getting there. |
| [Static Site Editor Vision Braindump](https://docs.google.com/document/d/1hMXxMw2YChL9l2CiurF8sffs01RDf1uzC4PppFQaQe4/edit#heading=h.h42owuydiis3) | If you're interested in all the various angles we explored in helping get to our current vision this document will give you some context. Note that its for context purposes only and not to be seen as a guide for what we will/wont do down the line. |

### Slack channels

The following channels are important to be a part of:
- [#g_create_static_site_editor](https://gitlab.slack.com/archives/CQ29L6B7T)
- [#g_create_static_site_editor_eng](https://gitlab.slack.com/archives/CU90JTCF8)
- [#g_create_static_site_editor_standup](https://gitlab.slack.com/archives/CUD52A59A)
- [#handbook](https://gitlab.slack.com/archives/C81PT2ALD)
- [#handbook-escalation](https://gitlab.slack.com/archives/CVDP3HG5V)
- [#master-broken-www-gitlab-com](#https://gitlab.slack.com/archives/C413US389)

The following Slack channels, while optional, are relevant to our group and you should consider joining them:
- [#engineering-dev](https://gitlab.slack.com/archives/CG7FPF4KT)
- [#s_create](https://gitlab.slack.com/archives/CQ08Z46N9)
- [#g_create-fe](https://gitlab.slack.com/archives/CGPEQCL23)

### PTO Ninja

We use PTO Ninja to notify and delegate for planned timeoff. When setting up your integrations with Slack,
be sure to visit the PTO Ninja Home tab and choose the Calendar Sync option from the dropdown.
Then add the team's shared Google Calendar `gitlab.com_56i46dodsa0mvtkfvn10hcssjo@group.calendar.google.com` in the "Additional Calendar to include?".

### Developer Cheatsheet

As a new developer at GitLab you might find these command helpful with your day to day coding. [View the Cheatsheet](/handbook/engineering/development/dev/create-static-site-editor/developer-cheatsheet/)

### Pair programming

We encourage pair programming not just as part of your onboarding into the team, but as a general good practice. Read our [guideline on pair programming](/handbook/engineering/development/dev/create-static-site-editor/pair-programming/).


## OKRs

The EM is responsible for the OKRs of the Static Site Editor group.

Current OKRs can be viewed here: https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/7630

### Team members

We use OKRs for team members to:
1. Ensure growth (both personally and for our product)
1. Ensure our efforts are aligned to common goals
1. Ensure we spread the load
1. Ensure we give focus to team members

We do **NOT** use OKRs at team member level for any sort of performance measurement and we do not report on it publicly.

The main themes for Q2 are:
1. Implement the knowledge we gain from using frontendmasters.com
1. Keep improving the Static Site Editor product
1. Reduce our support overhead for the Handbook and Docs projects

FY21 Q2 OKRs for the team members:

| Team Member        | Objective                                                                             | Key Results                                               |
|--------------------|---------------------------------------------------------------------------------------|-----------------------------------------------------------|
| Jacques Erasmus    | Implement Learnings                                                                   | A GraphQL feature improved or implemented in the product. |
|                    | Improve Static Site Editor product                                                    | Image uploading                                           |
|                    |                                                                                       | Overall styling alignment                                 |
|                    | Docs: Enable other GitLab team members to contribute easily                           | Modernize JS                                              |
|                    |                                                                                       | GitLab UI integration                                     |
| Enrique Alcantara  | Implement Learnings                                                                   | GraphQL API improved in the product                       |
|                    |                                                                                       | Become domain expert for GraphQL at GitLab                |
|                    | Improve Static Site Editor product                                                    | All REST API calls replaced with GraphQL API calls        |
| Vasilii Iakliushin | Implement Learnings                                                                   | A GraphQL API improvement landed in the product           |
|                    |                                                                                       | Become domain expert for GraphQL at GitLab                |
|                    | Improve Static Site Editor product                                                    | All REST API calls replaced with GraphQL API calls        |
| Derek Knox         | Implement Learnings                                                                   | GraphQL feature improved, implemented in the product      |
|                    |                                                                                       | AST knowledge gained implemented in the product           |
|                    | Improve Static Site Editor product                                                    | Effective handling of front matter in our product         |
|                    | Handbook: Assist in creating separation of Handbook from wider www-gitlab-com project | Bundling (CSS + JS) - Rollup                              |
| Chad Woolley       | Improve Static Site Editor product                                                    | Contribute first feature to the product                   |
|                    | Handbook: Assist in creating separation of Handbook from wider www-gitlab-com project | Monorepo structure in place                               |
|                    |                                                                                       | Project specific pipelines in place                       |
