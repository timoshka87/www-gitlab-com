---
layout: handbook-page-toc
title: Database Strategy
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}
## Database Strategy: Guidance for proposed database changes

GitLab is offered as a [Single Application](https://about.gitlab.com/handbook/product/single-application) with a [Single data-store](https://about.gitlab.com/handbook/product/single-application/#single-data-store).  This handbook entry is meant as guidance for when you encounter a situation where you are considering changes or additions to our data-store architecture.  For information on tooling, migrations, debugging and best practices please read the [Database guides](https://docs.gitlab.com/ee/development/#database-guides) section in [GitLab Docs](https://docs.gitlab.com/).

### Requirement
When you propose any database additions, updates or deletions it is required that you have participated in a [Database Review](https://docs.gitlab.com/ee/development/database_review.html#database-review-guidelines) prior to deployment (best early in development).  

### PostgreSQL 

The GitLab web app uses [PostgreSQL](https://docs.gitlab.com/ee/development/architecture.html#postgresql) for our persistent database imformation.  We have dropped support for [MySQL](https://gitlab.com/gitlab-org/gitlab-foss/-/issues/52442) and will be requiring PosgreSQL 11 with the release of GitLab 13.0.

#### When to consider another data-store

Over time there have been proposals to use different types of data-stores (e.g. NoSQL). At this point we are not considering adding another data-store for our persistent data.  Please read [Single data-store](https://about.gitlab.com/handbook/product/single-application/#single-data-store).

#### Single Data-store

We are intentionally requiring that the data for GitLab lives in a single data-store, in this case that means a single database.  The term "database" can be taken many different ways.  Here is how we are using the following terms:

- [database](https://www.postgresql.org/docs/11/manage-ag-overview.html) - A database is a named collection of SQL objects (“database objects”). Generally, every database object (tables, functions, etc.) belongs to one and only one database.
- [database cluster](https://www.postgresql.org/docs/8.1/creating-cluster.html) -  A database cluster is a collection of databases that is managed by a single instance of a running database server.
- [schema](https://www.postgresql.org/docs/8.1/ddl-schemas.html) - A database contains one or more named schemas, which in turn contain tables. Schemas also contain other kinds of named objects, including data types, functions, and operators. The same object name can be used in different schemas without conflict; for example, both schema1 and myschema may contain tables named mytable. Unlike databases, schemas are not rigidly separated: a user may access objects in any of the schemas in the database he is connected to, if he has privileges to do so.

##### Why a single database?

- Our [company strategy](https://about.gitlab.com/company/strategy/) is built around the [advantages of a single application](https://about.gitlab.com/handbook/product/single-application/).
  - Sid describes the importance of our single database strategy and the [Flywheel with two turbos](https://about.gitlab.com/company/strategy/#flywheel-with-two-turbos) in [this video](https://youtu.be/TGulb4sGJ9g?t=877).
- Our current efforts around database scalability are focusing on [database sharding](https://about.gitlab.com/company/team/structure/working-groups/database-sharding/) as a solution.

##### The trouble with microservices

Often, moving to a separate service or microservice is seen as a solution to a scaling or performance problem within the GitLab application.  However, moving to a microservice based solution may simply be defering the problem while creating a more complex architecture.  The articles and quotes below discuss real world examples of the struggles with microservices.  

- [To Microservices and Back Again - Why Segment Went Back to a Monolith](https://www.infoq.com/news/2020/04/microservices-back-again/)
>"If microservices are implemented incorrectly or used as a band-aid without addressing some of the root flaws in your system, you'll be unable to do new product development because you're drowning in the complexity."
- [Bad Reasons For Microservices](https://completedeveloperpodcast.com/episode-189/)
>Maintaining a bunch of loose microservices is not easier than maintaining a monolith. There might be an argument for moving from a distributed monolith to microservices, provided that the organizational problems that led to a distributed monolith have been addressed. A shift to microservices is likely to make things worse before it makes them better. If things are already bad, that’s going to make life difficult.

### Process for proposing a separate database

As we've learned from our discussions around [defining the container registry database schema](https://gitlab.com/gitlab-org/gitlab/-/issues/207147) there are times when it makes sense to separate the data out into its own database.  The following sections will describe what data points you should consider and how you would move forward in gaining approval to create a separate database.

##### Considerations

- Was the feature written in a completely separate code base?  Again, using the Container Registry example, this was written in Go and is separate from the main GitLab Rails application.
   - Only do a separate database when it has a completely separate codebase, and that is the only codebase querying the data. For example, [Product Analytics](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/27730) isn't fit for it since the Rails codebase touches it.
- Will the feature incur a heavy write load?  Writes are harder to scale than reads who can be scaled with replicas.
- Will the feature not need any access to the main Rails database?  Example, the data is completely isolated from the main Rails database and will not need to share data across databases.
- Are there existing APIs that we need to support?  For example, the container registry has a full set of APIs (https://docs.docker.com/registry/spec/api/) that we already use in the existing GitLab Rails application to retrieve tags, etc.


##### Approval Process

If you answered yes to all of the questions in the Considerations section above, then you will need to get approval for your separate database design before proceeding with implementation.

- Submit an issue with your design proposals
- Make the issue visible to the CEO- [Sid Sijbrandij](/company/team/#sytses) and all Fellow Engineers: currently [Stan Hu](/company/team/#stanhu) and [Dmitriy 'DZ' Zaporozhets](/company/team/#dzaporozhets)
- Label with ~"CEO Interest"
- You will need to gain approval from the CEO and one Fellow Engineer
  - Use the issue created in the first step to coordinate how to gain approval

