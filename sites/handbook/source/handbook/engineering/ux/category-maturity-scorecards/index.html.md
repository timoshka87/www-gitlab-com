---
layout: handbook-page-toc
title: "Category Maturity Scorecards"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Intro and Goal

The Category Maturity Scorecard is a [Summative Evaluation](https://www.nngroup.com/articles/formative-vs-summative-evaluations/) that takes into account the entire experience as defined by a Job To Be Done (JTBD), instead of individual improvement(s), which are often measured through Usability Testing. This process provides data to help Product Management grade the maturity of our product based on user performance.

The goal of this process is to produce data as objectively as possible given time and resource constraints. For this reason, the process is more rigorous than other UX research methods, and it focuses more on quantitative data (numbers and measures) and less on qualitative data (thoughts and verbal feedback). 

To produce data in which we have confidence, the data should be as free of subjective judgement as possible, relying on observed metrics and self-reported user sentiment. Our goal is to compare data over time to see how additions and improvements have impacted product maturity in a quantifiable way. To facilitate this, we've made this process prescriptive, so that it can be consistently applied by all Product Designers in all areas of our product.

You should conduct this process after a feature/function has already gone through problem and solution validation, so you have information about the ‘why’ behind any failures to complete the associated task(s). 

**Note:** As with any evaluation, it's always a good idea to run a pilot first, so you can identify any improvements needed in the research approach.

**Note:** If you have questions, suggestions for improvement, or find this process doesn’t meet the needs of your users or product, reach out to the UX Researcher for your group.

## Job(s) to be Done (JTBD)

Category Maturity Scorecards are about judging the quality of experiences against a defined and verified JTBD. Each Product Designer works with their Product Manager to identify Main JTBD(s) and Related JTBD(s) within the category, which are mapped to features. JTBD(s), along with the features they address, are used to create scenarios for the Category Maturity Scorecard process. The number of scenarios used often depends on the complexity of the features tested. 

Refer to the JTBD page (link coming soon) for information on how to use JTBD in research, including Category Maturity Scorecards.

Refer to the [Category Maturity](https://about.gitlab.com/direction/maturity/) page to understand scoring (updated content coming soon). It is important to note that:

* **Minimal:** Category Maturity Scorecard is *not* required.
* **Viable:** Category Maturity Scorecard is conducted with internal users on the primary JTBD only.
* **Complete and Lovable:** Category Maturity Scorecard is conducted with external users on the primary *and* secondary JTBDs. 

## Step 1: Define and recruit users

During the JTBD creation and validation phases, the Product Designer and Product Manager devise a set of user criteria to describe the user(s) you're referencing in your job(s). Using this criteria when recruiting for the Category Maturity Scorecard will ensure you are gathering feedback from the right type of user(s).  

To balance expediency with getting a variety of perspectives, we conduct the Category Maturity Scorecard research with five participants from one set of user criteria. If you have multiple user types for your JTBD, it is ideal to recruit 5 from each user type. Example: A JTBD can be completed by a DevOps Engineer and a Release Manager. In this case, you’d recruit a total of 10 participants: 5 DevOps Engineers and 5 Release Managers.

The recruiting criteria can be based on an existing persona, but needs to be specific enough to be turned into a screener survey. [Create a screener survey in Qualtrics](/handbook/engineering/ux/ux-research-training/recruiting-participants/#craft-your-screener-user-interviews-and-usability-testing) that your prospective participants will fill out to let you know if they're eligible. 

The template survey includes a question asking people if they consent to having their session recorded. Due to the analysis required for Category Maturity Scorecards, participants must answer yes to this question in order to participate. Once your screener survey is complete, open a [Recruiting request issue](https://gitlab.com/gitlab-org/ux-research/-/blob/master/.gitlab/issue_templates/Recruiting%20request.md) in the [UX Research project](https://gitlab.com/gitlab-org/ux-research/), and assign it to the relevant [Research Coordinator](https://about.gitlab.com/company/team/?department=ux-research-team). The Coordinator will review your screener, reach out to you if they have any issues, and begin recruiting users based on the timeline you give them.

Recruiting users takes time, so be sure to open the recruiting issue at least 2-3 weeks before you want to conduct your research. 

## Step 2: Prepare your testing environment

Testing in a production environment is the best choice, because your goal is to evaluate the actual product, not a prototype that may have slightly different experience.

Once you know what scenario(s) you’ll put your participants through, it’s important to determine the interface you’ll use. Some questions to ask yourself:

* **Can your participant use their own GitLab account?** If not, can you set them up with a GitLab.com account and have them use that? 
* **Do you require a self-managed instance?** If yes, do you need to provide one?
* **Do your scenarios require any external actions?** For example, do you need to display a specific alert, or can a participant complete everything on their own?
* **Does your scenario require interacting with anything besides a GitLab web-based interface?** Should they receive an email or do they need to use a command-line interface?

It’s important to thoroughly plan how a participant will complete your scenario(s), especially if you answered "yes" to any of the questions above. Involve technical counterparts early in the process if you have any uncertainty about how to enable users to go through your desired workflow(s).

If your JTBD interacts with other stage groups’ areas, reach out to them to ensure their part of our product will support your scenario(s). 

Because this is a summative evaluation of the current experience, all of the available options the participant should need access to must be available in the GitLab instance. When you recruit users, keep in mind the tools and features they must access to complete the JTBD scenarios/tasks.

## Step 3: Document success/failure of your JTBD scenario(s)
After you decide on your scenario(s) and user criteria, it's important to go through the flow(s) yourself.

Typically, there is a primary flow that users will most often use and sometimes secondary flows that are less common or efficient. Document both the primary flow and secondary flow(s), as you will use these to define successful completion of the scenario(s). For each flow, document both the entire path taken and why you chose it. 

**Sample primary path:**
1. Log into GitLab
2. Click ‘Projects’ in the top navigation
3. Click ‘UX Research’ in the project list
4. Click the ‘Create Issue’ button. 
5. Select ‘Research Request’ template```suggestion:-0+0
5. Select the ‘Research Request’ template.
6. Fill out information in the template.
7. Click the ‘submit issue’ button.

**Sample secondary path**

1. Log into GitLab.
2. Click in the ‘Projects’ search field.
3. Enter ‘UX Research’ and press Enter.
4. Locate the ‘UX Research’ project.
5. Navigate to ‘Issues’ in the left navigation.
6. Click the ‘New issue’ button.
7. Select the ‘Research Request’ template.
8. Fill out information in the template.
9. Click the ‘submit issue’ button.

Having a detailed and documented scenario will help you determine whether your participant took the primary or a secondary successful path. It will also help indicate the point at which your participant deviated from the successful path and veered into the path of failure. Indicating points of failure will help identify areas of product improvement.

Once you've gone through your scenario(s) and documented the primary and, as needed, secondary path(s), have a co-worker complete the scenario as well. Ideally, this person won't be familiar with the scenario, so they don't have an expert-level understanding of how it works. Use this to uncover any issues with how you've formulated your scenario or how you have documented the path(s). As this is meant as a way to check your scenario/path(s) plan, it's ok to coach your co-worker a little, using this discussion to get to the heart of any problems your scenario or path may have.

## Step 4: Conduct the research
Because Category Maturity Scorecards are a standardized process, moderators should follow this [testing guide](https://docs.google.com/document/d/1UtOD9j19VB0GzO2xAgXxCg-FeVqCA0GiFTTKVtMT4f0/edit?usp=sharing) as closely as possible. The moderator will typically be a Product Designer, but this is not strictly required. You are encouraged to have any relevant stakeholders attend the sessions, but it is very important they remain silent.

## Step 5: Analyze your results
The goal for analyzing Category Maturity Scorecard data is to establish a baseline measure for the current experience as it relates to the JTBD(s). Over time, our product will change as new features/functions are added/changed. We can review the data collected here to understand how these changes impacted the user experience and use it to make improvements.

#### Post-session debriefing
It’s important that the moderator and any stakeholders don’t leave the call when the session concludes. Instead, remove the participant and remain on the call. Use this time for the group to debrief on what they just experienced. The notetaker(s) should take notes on this discussion.

* Have each person talk about what they feel were the major findings of the session.
* Mention any issues with the session or things that should be done differently in the future.
    * For Category Maturity Scorecards, no changes can be made to how you conduct the sessions, otherwise the data can’t be compared. For this reason, we suggest running a pilot session to work out any kinks.  
    * Allow anyone to ask any questions about the content covered or otherwise say things they feel need to be said before the session concludes.

#### Resulting data
By following the Category Maturity Scorecard [testing guide](https://docs.google.com/document/d/1UtOD9j19VB0GzO2xAgXxCg-FeVqCA0GiFTTKVtMT4f0/edit?usp=sharing), you will have the following measures to report, per feature, not per scenario. However, scenarios may include more than one feature.

- **UMUX Lite scores:** Industry-standard scores for capabilities and ease of use (use these scores to determine category maturity)
- **Success/failure:** How many participants were successful or failed when completing the task? (use for future improvements)
- **Time on task:** How long did each participant take to complete the task? (use for future improvements)
- **Error rate:** Number of errors each participant encountered while attempting to complete the task (use for future improvements)

#### How to apply and score the UMUX  Lite
The [UMUX Lite](https://measuringu.com/umux-lite/) score is based on the UMUX (Usability Metric for User Experience), created by [Finstad](http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.872.6330&rep=rep1&type=pdf), and it is highly correlated with the SUS and the Net Promoter Score. It's intended to be similar to the SUS, but it's shorter and targeted toward the [ISO 9241 definition of usability](https://www.w3.org/2002/Talks/0104-usabilityprocess/slide3-0.html) (effectiveness, efficiency, and satisfaction). The UMUX was shortened by [Lewis et al.](https://link.springer.com/chapter/10.1007/978-3-319-20886-2_20), so that the content of the UMUX Lite nicely mimics the [Technology Acceptance Model](https://en.wikipedia.org/wiki/Technology_acceptance_model) (TAM)

When you apply the UMUX Lite, you'll ask the following questions of each participant using a 5-point scale (1 is lowest, 5 is highest):
* [This system's] capabilities meet my requirements.
* [This system] is easy to use.

Compute the average score for each question separately.
For example:

|   | P1  | P2  | P3  | P4  | P5  |
|---|:-:|:-:|:-:|:-:|:-:|
| The issue creation capabilities met my requirements.  | 4  | 2  |  3 | 5  | 5  |
| Issue creation is easy to use.  | 5  |  4 | 5  | 5  | 5  |

* The average score for issue creation capabilities - 3.8. (19/5)
* The average score for issue creation ease of use - 4.8 (24/5)

## Step 6: Document your findings

Use [Dovetail](https://about.gitlab.com/handbook/engineering/ux/dovetail/) to document your findings. Your notes should include the following information (you can create a note taking template for your Dovetail project to make this easier):

- Job to be done that was tested

**Scenarios**

Scenario 1

- Prompt - (add scenario prompt here)
- Average UMUX Lite score for capabilities - (score)
- Average UMUX Lite score for ease of use - (score)
- How many participants were successful at the task - (number of successful participants/out of total participants)
- How many participants failed the task - (number of participants who failed/out of total participants)
- Total number of errors each participant encountered while attempting to complete the task/scenario - (number of errors)

| Participant Number  | Successful  | Failed  | Number of Errors Encountered  |
|:-:|---|---|---|
| P1  |   |   |   |
| P2  |   |   |   |
| P3  |   |   |   |
| P4  |   |   |   |
| P5  |   |   |   |



Scenario 2

- Prompt - (add scenario prompt here)
- Average UMUX Lite score for capabilities - (score)
- Average UMUX Lite score for ease of use - (score)
- How many participants were successful at the task - (number of successful participants/out of total participants)
- How many participants failed the task - (number of participants who failed/out of total participants)
- Total number of errors each participant encountered while attempting to complete the task/scenario - (number of errors)

| Participant Number  | Successful  | Failed  | Number of Errors Encountered  |
|:-:|---|---|---|
| P1  |   |   |   |
| P2  |   |   |   |
| P3  |   |   |   |
| P4  |   |   |   |
| P5  |   |   |   |



(continue with as many scenarios as you used)

**Participants**

Participant 1

- Role -
- Top 3 tasks/responsibilities - 
- Previous/current GitLab usage -
- (add the video from the session)


Participant 2

- Role -
- Top 3 tasks/responsibilities - 
- Previous/current GitLab usage -
- (add the video from the session)

(continue with as many participants as you had in the study)

**Other notes**
(Add any additional notes that came from freeform discussion or elsewhere)

### Document recommendations

Read the UX Research team’s guide for [documenting insights in Dovetail](https://about.gitlab.com/handbook/engineering/ux/dovetail/#the-ux-research-teams-guide-to-documenting-insights-in-dovetail).
