---
layout: handbook-page-toc
title: "Data Team"
description: "GitLab Data Team Handbook"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .toc-list-icons .hidden-md .hidden-lg}

{::options parse_block_html="true" /}

----

## <i class="fab fa-gitlab fa-fw color-orange font-awesome" aria-hidden="true"></i>Quick Links
[Primary Project](https://gitlab.com/gitlab-data/analytics/){:.btn .btn-purple}
[Issue Tracker](https://gitlab.com/groups/gitlab-data/-/issues){:.btn .btn-purple}
[Issue Intake](https://about.gitlab.com/handbook/business-ops/data-team/#-data-analysis-process){:.btn .btn-purple}
[Sisense](https://app.periscopedata.com/app/gitlab/){:.btn .btn-purple}

| **SERVICES** | **SELF-SERVICE** | **TECH GUIDES** |  **INFRASTRUCTURE** |
| --- | --- | --- | --- |
| [KPI Index](/handbook/business-ops/data-team/kpi-index/#gitlab-kpis/) | [SiSense](/handbook/business-ops/data-team/platform/periscope/) | [SQL Style Guide](/handbook/business-ops/data-team/platform/sql-style-guide/) | [Data Platform](/handbook/business-ops/data-team/platform/) | 
| [KPI Development](/handbook/business-ops/data-team/kpi-index/index.html#kpi-development) | [Snowflake EDW](/handbook/business-ops/data-team/platform/#warehouse-access) |  [dbt Guide](/handbook/business-ops/data-team/platform/dbt-guide/) | [CI Jobs](/handbook/business-ops/data-team/platform/ci-jobs/) |
| [Data Analysis](/handbook/business-ops/data-team/#-data-analysis-process/) | [Enterprise Dimensional Model](/handbook/business-ops/data-team/platform/edw/)| [Python Style Guide](/handbook/business-ops/data-team/platform/python-style-guide/) | [Permifrost](/handbook/business-ops/data-team/platform/permifrost/) |
| [SheetLoad](/handbook/business-ops/data-team/platform/#using-sheetload/) | [Data Sources](/handbook/business-ops/data-team/platform/#extract-and-load/) | [Airflow & Kubernetes](/handbook/business-ops/data-team/platform/infrastructure/#common-airflow-and-kubernetes-tasks/)  | [Snowplow](/handbook/business-ops/data-team/platform/snowplow/) 
| [Special Event On Call](/handbook/business-ops/data-team/data-service/index.html#special-event-on-call/) | [Data for Product Managers](/handbook/business-ops/data-team/programs/data-for-product-managers/) | [Docker](/handbook/business-ops/data-team/platform/infrastructure/#docker) | [Data Infrastructure](/handbook/business-ops/data-team/platform/infrastructure/) |

----

🌎📚💻🏁🛠🔍👑📊 

Many of the sections on this page will have emojis just below the heading. For more information about what they convey [read the documentation page.](/handbook/business-ops/data-team/documentation)

----

<div class="alert alert-purple center">
  <p class="purple center" style="font-size: 34px ; text-align: center ; margin: auto">
    <strong>We <i class="fab fa-gitlab orange font-awesome" aria-hidden="true"></i> Data</strong>
  </p>
</div>
{:.no_toc}

## <i class="fas fa-seedling fa-fw icon-color font-awesome" aria-hidden="true"></i>Charter 
[🌎💡](/handbook/business-ops/data-team/documentation)

The Data Team is a Sub-department of the [Business Operations](https://about.gitlab.com/handbook/business-ops/) Department and provides a Data & Analytics platform, programs, and services to the entire company. 

## Mission
**Deliver Results That Matter With Trusted and Scalable Data Solutions**
<br>The Data Team strives to deliver high quality results that make a strategic impact with data solutions that can grow quickly and easily.

## Responsibilities
* Define and publish a Data Strategy to help maximize the value of GitLab's Data Assets
* Build and maintain the company's central Enterprise Data Warehouse to support Reporting, Analysis, [Dimensional Modeling](https://www.kimballgroup.com/data-warehouse-business-intelligence-resources/kimball-techniques/dimensional-modeling-techniques/), and Data Development for all GitLab teams
* Integrate new data sources to enable analysis of subject areas, activities, and processes
* Manage and govern the company's [Key Performance Indicator](https://about.gitlab.com/handbook/ceo/kpis/) definitions, database, and data visualizations
* Build and maintain an Enterprise Dimensional Model to enable [Single Source of Truth](https://en.wikipedia.org/wiki/Single_source_of_truth) results
* Develop Data Management features such as master data, reference data, data quality, data catalog, and data publishing
* Support the company's governance, risk, and compliance programs as they relate to Data & Analytics systems
* Provide Self-Service Data capabilities to help everyone leverage data and analytics
* Define and champion Data Quality practices and programs for all GitLab data systems
* Provide customizable Data Services, including Data Visualization, Data Modeling, Data Quality, and Data Integration
* Broadcast regular updates about data deliverables, ongoing initiatives, and upcoming plans

---
## <i class="far fa-compass fa-fw color-orange font-awesome" aria-hidden="true"></i>Data Team Principles 
[🌎💡](/handbook/business-ops/data-team/documentation)

The Data Team at GitLab is working to establish a world-class data analytics and engineering function by utilizing the tools of DevOps in combination with the core values of GitLab.
We believe that data teams have much to learn from DevOps.
We will work to model good software development best practices and integrate them into our data management and analytics.

A typical data team has members who fall along a spectrum of skills and focus.
For now, the data function at GitLab has Data Engineers and Data Analysts; eventually, the team will include Data Scientists. Review the [team organization section](/handbook/business-ops/data-team/#-team-organization) section to see the make up of the team.

Data Engineers are essentially software engineers who have a particular focus on data movement and orchestration.
The transition to DevOps is typically easier for them because much of their work is done using the command line and scripting languages such as bash and python.
One challenge in particular are data pipelines.
Most pipelines are not well tested, data movement is not typically idempotent, and auditability of history is challenging.

Data Analysts are further from DevOps practices than Data Engineers.
Most analysts use SQL for their analytics and queries, with Python or R.
In the past, data queries and transformations may have been done by custom tooling or software written by other companies.
These tools and approaches share similar traits in that they're likely not version controlled, there are probably few tests around them, and they are difficult to maintain at scale.

Data Scientists are probably furthest from integrating DevOps practices into their work.
Much of their work is done in tools like Jupyter Notebooks or R Studio.
Those who do machine learning create models that are not typically version controlled.
Data management and accessibility is also a concern.

We will work closely with the data and analytics communities to find solutions to these challenges.
Some of the solutions may be cultural in nature, and we aim to be a model for other organizations of how a world-class Data and Analytics team can utilize the best of DevOps for all Data Operations.

Some of our beliefs are:

* Everything can and should be defined in code
* Everything can and should be version controlled
* Data Engineers, Data Analysts, and Data Scientists can and should integrate best practices from DevOps into their workflow
* It is possible to serve the business while having a high-quality, maintainable code base
* Analytics, and the code that supports it, can and should be open source
* There can be a single source of truth for every analytic question within a company
* Data team managers serve their team and not themselves
* [Glue work](https://www.locallyoptimistic.com/post/glue-work/) is important for the health of the team and is recognized individually for the value it provides. [We call this out specifically as women tend to over-index on glue work and it can negatively affects their careers.](https://noidea.dog/glue)
* We focus our limited resources where data will have the greatest impact
* Lead indicators are just as important, if not moreso, than lag indicators
* All business users should be able to learn how to interpret and calculate simple statistics

---

## Navigating the Data Team Handbook

- [KPI Index](/handbook/business-ops/data-team/kpi-index)
- [How We Work](/handbook/business-ops/data-team/how-we-work)
	- [Calendar](/handbook/business-ops/data-team/how-we-work/calendar)
	- [Team Duties](/handbook/business-ops/data-team/how-we-work/duties)
- [Data Team Organization](/handbook/business-ops/data-team/organization)
- [Data Platform](/handbook/business-ops/data-team/platform)
	- [Sisense (Periscope)](/handbook/business-ops/data-team/platform/periscope)
	- [dbt Guide](/handbook/business-ops/data-team/platform/dbt-guide)
	- [Enterprise Data Warehouse](/handbook/business-ops/data-team/platform/edw)
	- [Data Infrastructure](/handbook/business-ops/data-team/platform/infrastructure)
	- [SQL Style Guide](/handbook/business-ops/data-team/platform/sql-style-guide)
	- [Python Style Guide](/handbook/business-ops/data-team/platform/python-style-guide)
	- [Permifrost](/handbook/business-ops/data-team/platform/permifrost)
	- [Snowplow](/handbook/business-ops/data-team/platform/snowplow)
	- [Data CI Jobs](/handbook/business-ops/data-team/platform/ci-jobs)
- [Data Learning and Resources](/handbook/business-ops/data-team/learning-library)

---

## <i class="fas fa-bullhorn fa-fw color-orange font-awesome" aria-hidden="true"></i>Contact Us

* [Data Team Project](https://gitlab.com/gitlab-data/analytics/)
* [#data](https://gitlab.slack.com/messages/data/) on Slack
* [What time is it for folks on the data team?](https://www.worldtimebuddy.com/?pl=1&lid=2950159,6173331,4487042,4644585&h=2950159)

#### Slack 
[🌎📚](/handbook/business-ops/data-team/documentation)

The Data Team uses these channels on Slack:

* [#data](https://gitlab.slack.com/messages/data/) is the primary channel for all of GitLab's data and analysis conversations. This is where folks from other teams can link to their issues, ask for help, direction, and get general feedback from members of the Data Team.
* [#data-daily](https://gitlab.slack.com/messages/data-daily/) is where the Data Team tracks day-to-day productivity, blockers, and fun. Powered by [Geekbot](https://geekbot.com/), it's our asynchronous version of a daily stand-up, and helps keep everyone on the Data Team aligned and informed.
* [#data-lounge](https://gitlab.slack.com/messages/data-lounge/) is for links to interesting articles, podcasts, blog posts, etc. A good space for casual data conversations that don't necessarily relate to GitLab.
* [#business-operations](https://gitlab.slack.com/messages/business-operations/) is where the Data Team coordinates with Business Operations in order to support scaling, and where all Business Operations-related conversations occur.
* [#analytics-pipelines](https://gitlab.slack.com/messages/analytics-pipelines/) is where slack logs for the ELT pipelines are output and is for data engineers to maintain.  The DRI for tracking and triaging issues from this channel is shown [here](/handbook/business-ops/data-team/platform/infrastructure/#data-infrastructure-monitoring-schedule)
* [#dbt-runs](https://gitlab.slack.com/messages/dbt-runs/), like #analytics-pipelines, is where slack logs for [dbt](https://www.getdbt.com/) runs are output. The Data Team tracks these logs and triages accordingly.
* [#data-triage](https://gitlab.slack.com/messages/data-triage/) is an activity feed of opened and closed issues and MR in the data team project.