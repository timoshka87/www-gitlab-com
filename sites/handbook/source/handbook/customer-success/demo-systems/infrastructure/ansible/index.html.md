---
layout: handbook-page-toc
title: "Demo Systems Infrastructure - Ansible"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Overview

We use Ansible for managing all of our configurations on Linux hosts after they have been deployed using Terraform. 

Most of Ansible configuration files use YAML syntax, however the standard folder organization for Ansible can seem confusing at first. It is strongly recommended to skim (not read) the Ansible documentation to understand the syntax and organization of Ansible code. 

See the README in the repository to learn more about our implementation and how to use Ansible.

## Repository

[https://gitlab.com/gitlab-com/customer-success/demo-systems/infrastructure/demosys-ansible](https://gitlab.com/gitlab-com/customer-success/demo-systems/infrastructure/demosys-ansible)
