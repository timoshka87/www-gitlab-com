---
layout: handbook-page-toc
title: "Finance"
---

## Welcome to the GitLab Finance Handbook
{: .no_toc}

The GitLab Finance team includes multiple functional groups: Accounting, Financial Planning & Analysis, Legal, Tax and Business Operations Programs.
{: .note}

# On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .toc-list-icons .hidden-md .hidden-lg}

## <i class="fab fa-gitlab fa-fw icon-color font-awesome" aria-hidden="true"></i> Quick Links

[Finance Issues](https://gitlab.com/gitlab-com/finance/issues){:.btn .btn-purple-inv}
- Please use confidential issues for topics that should only be visible to team members at GitLab.

[Contract and Payment Approval Process](/handbook/finance/procure-to-pay){:.btn .btn-purple-inv}

[Operating Metrics](/handbook/finance/operating-metrics/){:.btn .btn-purple-inv}

[Sales Comp Plan](/handbook/finance/sales-comp-plan/){:.btn .btn-purple-inv}

[Signature Authorization Matrix](/handbook/finance/authorization-matrix){:.btn .btn-purple-inv}

[Stock Options](/handbook/stock-options/){:.btn .btn-purple-inv}

[Travel and Expense Guidelines](/handbook/finance/accounting/#travel-and-expense-guidelines){:.btn .btn-purple-inv}

## Forms

[Link to W9 Doc](https://drive.google.com/a/gitlab.com/file/d/1rYXpKfCTKES1iLif8gjQ_ApwXqA611ic/view?usp=sharing){:.btn .btn-purple-inv}

[Link to form CA-590](https://drive.google.com/a/gitlab.com/file/d/0BzE3Rq8kSQ6Tcmp3a19xcFBZOWs/view?usp=sharing){:.btn .btn-purple-inv}

## Correspondence 

- Please use the `#finance` chat channel in Slack for questions that don't seem appropriate for the issue tracker or internal email correspondence.

- **Accounts Payable**- inquiries which relate to vendor and merchant invoices should be sent to our Accounts Payable mailbox - ap@gitlab.com and invoices that require payment processing should be sent to gitlab@supplierinvoices.com, electronic copies of all invoices should be sent to this address as soon as they are billed.

- **Accounts Receivable**- customer billing inquiries should be sent to our Accounts Receivable mailbox – ar@gitlab.com

- **Payroll**- inquiries which relate to contractor invoices should be sent to our Payroll mailbox - nonuspayroll@gitlab.com.

----

<div class="alert alert-purple center"><h3 class="purple"><strong>We <i class="fas fa-heart orange font-awesome" aria-hidden="true"></i> Finance</strong></h3></div>

## <i class="fas fa-book fa-fw icon-color font-awesome" aria-hidden="true"></i> Finance Handbooks
{: #finance-handbooks}

- [Accounting](/handbook/finance/accounting/)
- [Business Operations](/handbook/business-ops/)
- [Financial Planning & Analysis](/handbook/finance/financial-planning-and-analysis/)
- [Investor Relations](/handbook/finance/investor-relations/)
- [Legal](/handbook/legal/)
- [Tax](/handbook/tax/)

----

## General Topics

### Legal and Financial Information

For commonly requested company information, please visit our [wiki page](https://gitlab.com/gitlab-com/finance/wikis/company-information).

### Company Accounts
<a name="company-accounts"></a>

Login information for the following accounts can be found in the Secretarial vault
on 1Password:

- FedEx
- Amazon
- IND (Immigratie en Naturalisatie Dienst, in the Netherlands) company number

If you need this information but cannot find it in your shared vaults, check with the People Operations Specialists to get access.

### Fiscal Year
<a name="fiscal-year"></a>
GitLab's Fiscal Year runs from February 1 to January 31.

* Q1: February 1 through April 30
* Q2: May 1 through July 31
* Q3: August 1 through October 31
* Q4: November 1 through January 31

Refer to our [writing style guidelines]("/handbook/communication/#writing-style-guidelines") for proper use of date formats.

## Invoice template and where to send
<a name="invoices"></a>

Vendor invoices are to be sent to gitlab@supplierinvoices.com and payroll@gitlab.com for contractors. An [invoice
template](https://docs.google.com/spreadsheets/d/1sRA2uCpFblOleyVIslqM4YwbW27GkU5DTgwMLhgR_Iw/edit?usp=sharing) can be found in Google Docs by the name of "Invoice Template".

Non-US contractors not from Hungary, Italy, Japan, New Zealand, Nigeria, South Africa, Switzerland, and Ukraine will get pay their monthly wages and expenses through iiPay.  Here is the process:
 - enter bank information in BambooHR under Bank Information tab.  Note - new contractor should enter their bank information within 3 business days from their start date. 
 - enter VAT number if it is available
 - submit monthly salary/bonus/commission invoice in BambooHR under Contractor Invoice tab (select the currency based on contract agreement) by the 8th of each month
 - submit expenses through Expensify by the 8th of each month
 - the scheduled payment date is the 22nd of each month
 - iiPay will send out remittance advice along with the scheduled payment via your gitlab email address

Non-US contractors from Hungary, Italy, Japan, New Zealand, Nigeria, South Africa, Switzerland, and Ukraine will need to send their monthly invoice for salary/bonus/commission and any [expenses](/handbook/spending-company-money/) by the 15th to payroll@gitlab.com.

In many cases, VAT will not be payable on transactions between GitLab BV and EU-based
vendors/contractors, thanks to "Shifted VAT". To make use of this shifted VAT:

* The vendor/contractor writes the phrase "VAT shifted to recipient according to
article 44 and 196 of the European VAT Directive" on the invoice along with the
VAT-number of GitLab BV (NL853740343B01).
* On the vendor's VAT return the revenue from GitLab BV goes to the rubric "Revenue within the EU". It goes without saying that vendors are responsible for their own correct and timely filings.
* GitLab BV files the VAT on the VAT return, and is generally able to deduct this VAT all as part of the same return.

### Timesheets for Hourly Employees

1. People Ops and Finance will share a private Google Sheet with you where you will log your hours for each day in the “hours” column.
1. There is a dropdown in the “pay type” column, with the default being Regular. There are also options for Overtime, Vacation, Sick, and Bereavement. Choose the appropriate pay type for your time.
1. If you work overtime or more hours than agreed upon in your contract, please obtain approval from your manager and forward to Finance before payroll cutoff.
1. Your timesheet is due one day prior to the submit payroll date, which is outlined for the calendar year on your timesheet.

### How spend is allocated to departments

Budgets are aligned to departments, and budgets follow the owner who requested the spend. When determining who should request the spend, consider the owner of the outcome. 

For example, work to enable a sales partner that will not be incorporated into the product would go to Sales. Work that will be part of the product and supported by engineering, should be charged to the Engineering budget.

### Finance Business Partner Alignment

| Function | Name |
| -------- | ---- |
| Customer Support | Brooks Royer |
| R & D | Brooks Royer |
| Sales | Frederick Kurniadi and Yi Sun |
| Professional Services | Frederick Kurniadi and Sid Malik |
| Marketing | Christine Machado and Cristal McClure |
| G & A | Christine Machado and Cristal McClure | 
