---
layout: handbook-page-toc
title: "Travel"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}



## Expenses While Traveling <a name="expenses-while-traveling"></a>

1. The company will cover all work-related travel expenses. This includes lodging and meals during the part of the trip that is work-related. Depending on the distance of your travel, it can also include one day before and one day after the work related business. For example, if you are attending a 3 day conference in a jetlag-inducing location, the company will cover your lodging and meals those 3 days as well as one day before and one day after.
1. The company will cover costs related to transiting to and from a destination or airport which is work-related. This includes public transit, ride-sharing services (Lyft, Uber, etc.), or personal vehicle mileage between one's home and airport, bus/rail station, or work-related meeting. 
   * When expensing personal vehicle mileage within [Expensify](/handbook/spending-company-money/#expense-reimbursement), navigate to `New Expense`, `Manually Create`, and select the `Distance` tab for [automatic calculation and map receipt creation](/images/handbook/travel/expensify-personal-vehicle-mileage-reimbursement-distance-tab-example.png). 
1. The company can accommodate custom requests. It is OK to stay longer on your trip. However, the extra days will not be covered by the company.
1. Understand that the guidelines of [Spending Company Money](/handbook/spending-company-money), especially the part about spending it like it is your own money, still apply for all expenses.
1. Always bring a credit card with you when traveling for company business if you have one.
1. Hotels will generally expect you to have a physical credit card to present upon check-in. This credit card will be kept on file for the duration of your stay. Even if your lodging was pre-paid by the company or by using a company credit card, the Hotel may still require a card to be on file for "incidentals".
1. If you incur any work-travel related expenses (on your personal card or a GitLab company card), please make sure to save the original receipt.
1. When your trip is complete, please file an expense report via Expensify or include the receipts on your next monthly invoice.

## Self Stay Incentive Policy <a name="self-stay"></a>

If you decide to stay with friends or family instead of a hotel / Airbnb for a business trip, the company will pay you 50% of what would have been
the cost of a reasonable hotel. A reasonable hotel would be the negotiated hotel rate a conference provides.  This is in keeping with our values of frugality and freedom, and intended to reward you for saving company money on
accommodation expenses. Contractors should take a screenshot of a hotel search from the same location in TripActions and submit this along with your expense report or invoice.  Employees will be paid through payroll by submitting the screenshot by email to People Operations for processing.

## Booking travel and lodging<a name="TripActions"></a>

Before booking any flights please ensure that you have the proper [visas](/handbook/people-group/visas/#arranging-a-visa-for-travel-) in place.

When your accommodation booking is confirmed, you may want to communicate the dates and address with your manager.

### Setting up your TripActions account<a name="setup-tripactions"></a>

Below is some more information to help you get set up with your [TripActions](https://tripactions.com/signup) account.  

1. Follow the link in your onboarding issue to create your TripActions account and set a secure password. (Contact People Ops if you need help finding the link).
1. Once you are in your dashboard, make sure to set up your info in your profile (see link from drop down menu under your name).
1. Add your passport information to your profile (only passport information is accepted), this will populate your info directly when booking a flight.
1. Now let's start booking!

### Booking travel through TripActions <a name="booking-travel"></a>

**Flights**

All flights need to be booked through [TripActions](https://tripactions.com) (Business related flights), the costs will be added to the GitLab invoice and **no credit card is needed**. Please note that all business related flights will not have the option to pay with a personal credit card. Also to note that while it is possible to find a cheaper flight outside of TripActions, we get overall better rates in the aggregate with the buying power of the whole company.

**Hotels and Cars**

If you book Hotels and/or Cars through TripActions, the costs will need to be charged to a personal credit card. For business related bookings, you can submit the costs for reimbursement via our corporate expense reimbursement tool. 

**Trains**

If you book Trains through TripActions, business related bookings can be added to the GitLab invoice and **no credit card is needed**. 

**Personal Bookings**

In TripActions, you have the ability to book Personal trips. Note that when selecting this option, a personal credit card will be required at time of booking. Personal trips will not have the option to be added to the GitLab invoice.

#### Basic economy in TripActions

Many airlines offer [basic economy](https://en.wikipedia.org/wiki/Basic_economy) fares by default. This can create confusion and stress on the traveler if the conditions of their airline ticket aren't known.

To ensure a trouble-free experience with air travel booked through TripActions:

- If basic economy conditions are unsuitable, [filter out basic economy fares](https://tripactions.com/blog/basic-economy-fares-in-tripactions) from search results.
- Ahead of time, using the value in the `CONFIRMATION` field in a TripActions trip entry, look up your flights with the airline's own website to ensure times and conditions are what you expect.
- Check in using the airline's website (not TripActions) as soon as check-in becomes available. This will make it clear ahead of time if checked baggage must be added to a flight at additional cost, and provide time to contact the airline in case of any other issues.

#### How to book

1. Please note that some budget airlines might not show, so, if you want that, make sure
to check those to be sure there is no better option out there. (SouthWest, JetBlue, etc.).
1. Login to your account. If you've not yet set up your account, do this [first](#setting-up-your-tripactions-account).
1. The dashboard gives you the option to start a booking directly. The options are
to book flights, hotels, trains, or rental cars.
1. Input info for the flight you want to book. You can pick return as a combo or each leg separately
1. Select "continue to checkout" and fill out the reasons for booking.
1. Once confirming all details are correct you can select "complete booking".
1. If your trip is out of policy, you will have the ability to continue booking the trip and a "soft" approval email will be sent to your direct manager. Please refer to "Booking shows Out of Policy" below.

1. You can add a hotel or car by clicking the boxes between the flight details and "complete booking" and go through the same steps.

Note: Once a flight is booked, re-routing is at person’s own cost unless requested by the company or force majeure (in the latter case often the airline will cover the difference).

#### FAQ about travel
Please read through these FAQ **entirely**.

1. If you have trouble finding travel within a budget, contact **your manager** about this, _not_ TripActions.
1. If your flight hasn't been approved, contact **your manager**, they are responsible, _not_ TripActions.
1. If your flight does not have a seat selection or checked bags included, you need to arrange that with **the airline**. Usually done upon check-in online, 24H before your flight.
1. If part of your travel has been changed after you've booked, contact **TripActions Support** about this, they can help.

##### FAQ regarding transition from NexTravel to TripActions
[TripActions FAQs](https://tripactions.com/faq#) is a great resource for questions surrounding TripActions features. 

1. Does TripActions integrate with Okta?
    * Yes! Access to TripActions is a baseline entitlement and should be automatically added to a team member's Okta profile.
1. Will we be able to use personal credit cards to pay for trips booked through TripActions?
    * Yes, you can add and maintain personal credit cards in your user profile under the "Payments & Clubs" tab. Once a personal credit card has been added, you can select this payment method at time of booking. **Please note that TripActions allows team members the ability to book "Personal Trips". If/When selecting a "Personal Trip", any "Corporate" payment method will not be available for use.**
1. Are we requiring all travel to be booked through this portal? Or is it still the same as before eg. book based on what works best in options and price?
    * No, we are still booking based on what works best in options and price.
1. Is there documentation on how to erase our account with NextTravel?
    * The Business Operations Team and Security Team are actively working on deleting PII data in NexTravel. 
1. I’d like to know if I can seamlessly integrate with booking personal travel as well - e.g. if I can book for my partner and I to be sat next to each other, for example, or that we are stopping in Canada first, etc.
    * TripActions has alot of neat features! You have the ability to choose the Multi-City as well as the option to choose your seats. From [TripActions FAQs](https://tripactions.com/faq#) **TripActions recently added the ability to pick your seat from all platforms (being a mobile first company, you used to have to use our mobile apps). Some airlines need the flight confirmation code verified before you can select a seat. Confirmation numbers are granted and verified after ticketing is complete. This means that once ticketing is complete you will need to return to the ‘My Trips’ section within TripActions to select your seat. We are continuously working with our partners as well as expanding our features and functionality to improve your experience.**

**Please also review our [TripActions Q&A doc](https://docs.google.com/document/d/1fDWBzJV3YjojZN5t7anPzJYixhCjpkiP9flWemJS2LM/edit?usp=sharing).**

#### Booking shows Out of Policy

- Sometimes there is no way around booking a flight or hotel that is out of policy.
- This means that the option selected is more expensive than the cheapest option + our buffer.
- When you try to book this, an email will be sent to your manager for approval.
- Please provide extensive reasoning to make approval easier and faster.
- After 24 hours without any manual action (approved/denied) the booking will automatically be accepted.

### Booking accommodations through Airbnb <a name="airbnb"></a>

- You can book Airbnb via TripActions! TripActions has a feature allowing you to view results based on Property Type.
- If you are booking the trip yourself, you may be reimbursed for the portion of your stay that was business related by submitting in Expensify or on your monthly invoice.

## Travel Insurance

GitLab offers travel insurance for business travel. Full details of GitLab's Business Travel Accident Policy can be found in the [benefits section](/handbook/total-rewards/benefits/general-and-entity-benefits/#business-travel-accident-policy).

## Tips on Working Remotely While Abroad

Working remotely while being abroad can be quite different. For example it can be harder to arrange good working internet and you may need extra tools to have a similar level of comfort to be able to work effectively.

Learn more about [optimizing comfort and efficiency when taking your office with you while traveling](/company/culture/all-remote/working-while-traveling/).

### Flights

Planning flights far in advance can help you get cheaper fares when booking. For example you can set alerts when there is a significant drop in price for your travel destination.

- [Google Flights](https://www.google.com/flights/) - Great for comparing and setting up alerts
- [Skyscanner](https://www.skyscanner.com) - Great for comparing and setting up alerts
- [Kiwi](https://www.kiwi.com/) - Great experience for booking flights on mobile devices
- [Airwander](http://airwander.com/) - Great for booking flights with stopovers

What if you find a cheaper flight outside TripActions? If you are able to find a better price elsewhere, feel free to contact `support@tripactions.com` or call `+1-(888)-505-TRIP (8747)` for help. Please note, TripActions cannot match flight pricing found on “discount websites” or “flash sales” that are sometimes advertised as part of an airline sale. They are bound to the prices set by the airlines.

### Navigation

Offline navigation content is key when you are not certain of an internet connection.

- [Maps.me](http://maps.me/en/home) - Large offline maps, lots of destination content
- [Google Maps](https://www.google.com/maps) - Google maps allows offline downloads for self selectable areas

### Communication

When you have an internet connection, it can save you quite a bit of money to be able to communicate just using data.

- [WhatsApp](https://www.whatsapp.com/) - Most used app, which supports chat and video/voice calling
- [Telegram](https://telegram.org/) - Great for places where WhatsApp isn't working
- [Slack](https://slack.com/)
- [Google Duo](https://duo.google.com/) - Great for video and/or voice calling, especially on low bandwidth connections

### Special Shoutouts

Application which are all around wonderful to have when traveling.

- [Google Now](http://www.google.nl/landing/now/) - an intelligent personal assistant developed by Google. Learn more about [Google Now on Wikipedia](https://en.wikipedia.org/wiki/Google_Now).
- [Google Trips](https://get.google.com/trips/) - Makes planning your day very easy with suggestions for things to see and do

### Handling Currencies

The following tools make paying someone back in their own currency a lot easier.

- [PayPal](https://www.paypal.com)
- [TransferWise](https://transferwise.com/)

### Splitting Costs

Traveling together or letting someone else pay something for you? This makes it easier to see how costs add up or should be split up.

- [Splitwise](https://www.splitwise.com/) - Supports a great amount of currencies and is well designed

### Internet Problems and VPNs

- In China most services we use daily are not working due to [the great firewall of China](https://en.wikipedia.org/wiki/Great_Firewall). WhatsApp seems to work though!

VPNs can help you reach services or sites that are blocked, get contents that are not available abroad or let you browse the internet as if you were home (with your own language preselected).

- [ZenMate](https://zenmate.com/) - Great application with a good 7 days free trial
- [Opera](http://www.opera.com/) - Has a built in free VPN

*Note that a VPN may not always work as intended. For example both Netflix and China have blocked certain VPNs, limiting their usefulness.*

### Media and Entertainment

The following services provide offline functionality for their content if you have a paid membership.

- [Spotify](https://www.spotify.com)
- [Soundcloud](https://soundcloud.com)
- [Netflix](https://netflix.com)

### Power

Electricity is a requirement on your journey abroad. Be it to power your phone or your laptop.

- [Anker powerbanks](https://www.anker.com/) - Great value for the price, including USB-C options
- [Skross world adapter pro](http://www.skross.com/en/product/87/world-adapter-pro.html) - All around great travel adapter, usable in almost every country

### Secure your data during travels

During your working travel your restricted data could be exposed.
If you feel that your travel frequency may expose your data please keep in mind the following points to ensure that sensitive data contained in your devices will not be compromised:

- VPN - If you are connecting from an untrusted network you should use a VPN connection to avoid [MITM Attack](https://en.wikipedia.org/wiki/Man-in-the-middle_attack) or similar.
- Devices in public places - If you are using your device in a public place someone could read restricted data from your screen, you should protect your screen with a special film that ensure your privacy, [here you can find some samples](https://www.amazon.com/s?k=privacy+screen+filter)
- Speaking in crowded places - Ensure that when you are talking about restricted data you are in a secure place and no-one can hear you.
- 1Password travel mode - If you are approaching travel in a risky country or you have to leave your devices in an insecure place, please use [Travel Mode](https://support.1password.com/travel-mode/) in 1Password to ensure that your vaults will be safe if your device is compromised.

## Travel Guidance: COVID-19

Our top priority is the health and safety of our team members. 

### International Travel
GitLab has suspended all cross-border business travel until further notice. If you have international travel booked, please contact your
travel service to cancel or update your itinerary. Instructions to assist you with this process can be found below. If you are currently traveling internationally, please return home as soon as practical. If you have future travel booked during the suspension period, please cancel.

### All Other Travel
GitLab has suspended all non-essential domestic business travel until further notice. All domestic travel beyond your regular remote work location is limited to business critical matters and requires permission from the Covid-19 Response Committee (which can be reached by team members via the #covid19-response-committee slack channel) until the suspension period has ended. Local small meetings in your regular remote work area should be avoided in favor of remote video meetings.

### Personal Travel
We encourage you to make your own decision about what is right for you regarding your personal travel. Please be aware that any risk of potential exposure to COVID-19 may subject you to potential restrictions from GitLab or third parties (ex. customers, vendors, conferences, etc.) on your ability to attend in-person meetings or events for some period of time, even after the removal of general travel restrictions. At this time, the CDC recommends you avoid all nonessential international travel. If you do choose to travel internationally,  please note the resources provided by the CDC (https://wwwnc.cdc.gov/travel/notices) and we ask that you notify your manager with your travel dates so we can help set expectations for timing to return to possible in-person meetings in the future. We will continue to monitor the COVID-19 situation and will update the handbook accordingly. 

### Business Essential
Travel is considered business essential if it is necessary to our continued operations or would cause damage to
GitLab or its customers if canceled. Please discuss your travel plans with your manager to determine whether your travel
should continue.

To request an exception:
1.  Talk to your manager and explain the rationale for your business essential travel
2.  Your manager will propose to the [E-group member](/handbook/leadership/#e-group) for your team who will, if supported, propose the trip for approval to the COVID-19 Committee via the slack channel #covid19-response-committee. Only requests from the E-Group member should be posted to the slack channel. 

How to route exception requests to E-Group:
1. After your manager has approved your business essential travel, please route the request to the appropriate E-Group member using the [communication guidelines and slack channels listed](/handbook/communication/#getting-in-touch-with-the-e-group). For approvals from the CRO, please use #cro-approvals in slack and @ mention the CRO with the exception request. The request should come from the CRO's direct report. 

### General recommendations/precautions: 
- Wash your hands frequently with soap and water or use an alcohol-based hand rub if your hands are not visibly dirty
- Practice good respiratory hygiene, that is, when coughing and sneezing, cover your mouth and nose with flexed elbow or tissue – discard tissue immediately into a closed bin and clean your hands with alcohol-based hand rub or soap and water
- Maintain physical distancing, that is, leave at least 2 metres (6 feet) distance between yourself and other people, particularly those who are coughing, sneezing and have a fever
- Avoid touching your eyes, nose and mouth – if you touch your eyes, nose or mouth with your contaminated hands, you can transfer the virus from the surface to yourself
- Keep the surfaces around you clean
 
### Resources: 
- [World Health Organization Guidance](https://www.who.int/emergencies/diseases/novel-coronavirus-2019)
- [CDC Guidance](https://www.cdc.gov/coronavirus/2019-ncov/travelers/index.html)
- [Government of Canada Guidance](https://www.canada.ca/en/public-health/services/diseases/2019-novel-coronavirus-infection.html)



