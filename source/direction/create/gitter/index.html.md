---
layout: markdown_page
title: "Category Direction - Gitter"
---

- TOC
{:toc}

## Gitter

|  Stage   |   Maturity  |   Content Last Reviewed   |
|  ---   |   ---   |   ---   |
| [Create](/direction/dev/index.html#create) | [Minimal](/direction/maturity/) | `2020-07-01` |

Gitter is an open source chat application for developers, and is the place to connect the open source and software development community.

Gitter is a popular tool for building/supporting open source communities and is well integrated with GitHub. Since GitLab acquired Gitter, the team has open sourced it's [web app](https://gitlab.com/gitlab-org/gitter/webapp), [iOS app](https://gitlab.com/gitlab-org/gitter/gitter-ios-app) and [Android app](https://gitlab.com/gitlab-org/gitter/gitter-android-app). We want Gitter to be the best place for building open source communities, particularly when the project is hosted on GitLab, and make it [available by default to projects](https://gitlab.com/groups/gitlab-org/-/epics/359). There are also exciting possibilities to use Gitter to streamline the transition from async communication in issues and merge requests to synchronous forms of communication like real-time chat. And [opportunities to transition some of the open source community over to GitLab](#business-opportunity).

- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Agitter)
- [Epic List](https://gitlab.com/groups/gitlab-org/-/epics?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Agitter)
- [Overall GitLab Vision](/direction/create/)

Please reach out to PM Eric Schurter ([E-Mail](mailto:eschurter@gitlab.com)
[Twitter](https://twitter.com/e_shirt)) if you'd like to provide feedback or ask
questions about what's coming.

## Target Audience and Experience

**Open source developers:** As Gitter was originally highly integrated with GitHub, there is an existing base of developers. This audience is valuable in order to position [GitLab as the most popular collaboration tool](https://about.gitlab.com/company/strategy/#big-hairy-audacious-goal). Helping people communicate to get their problems solved and questions answered.

**Open source project maintainers:** Be the place that project maintainers want to setup their community on. This means integrating with their project so the team can talk and reference easily and jump to where they want to go.

## What's Next & Why

**In Progress: [GitLab communities and rooms](https://gitlab.com/groups/gitlab-org/-/epics/398)** are needed to grow the user base of Gitter by making it work well with GitLab, which includes making it easy for [every project to have a Gitter community](https://gitlab.com/groups/gitlab-org/-/epics/359). This is step one to making the GitLab transition attractive for an existing project on Gitter. Support for GitLab communities and rooms is nearly complete, with only support for GitLab [user namespace communities](https://gitlab.com/gitlab-org/gitter/webapp/-/issues/2397) remaining.

**In Progress:** Moving to a [Progressive Web App](https://gitlab.com/gitlab-org/gitter/webapp/-/issues/1992) will align the Gitter mobile and desktop experiences around a single codebase and improve productivity for the team.

## Competitive Landscape

Our competitors have a lot more polish which makes them attractive. Gitter's competitive advantage is developer friendly and familiar Markdown formatting support, publicly viewable archives, and free without limits.

- [Slack](https://slack.com)
  - Lots of polish
  - Invite system and closed ecosystem which makes the information hidden and harder to jump in
  - Limited message archive unless you pay
- [Discord](https://discordapp.com/)
  - Lots of polish
  - More gamer centric but plenty of thriving variety communities
- [Mattermost](https://mattermost.com/)
  - Open source
  - Very competitive with Slack
  - Doesn't offer a free hosted SaaS tier which may be why we don't see it mentioned often
- [Spectrum](https://spectrum.chat/)
  - Developer focused
  - Not exactly chat but going after the same open source crowd and has big projects onboard
  - Question and answer forum style (but is realtime)
- IRC ([Freenode](https://freenode.net/))
  - Longstanding dead simple chat
  - Plenty of developer communities still using IRC

## Business Opportunity

Gitter is uniquely positioned to introduce open source developers to the GitLab ecosystem. With the existing open source community on Gitter being GitHub centric, any branding, [cross-references](https://gitlab.com/groups/gitlab-org/-/epics/402), or unique features for GitLab communities will make people more familiar with GitLab. The ideal outcome to make the switch over to GitLab obvious.

## Top Customer Success communities

- **[FreeCodeCamp](https://gitter.im/FreeCodeCamp/home):** Free courses on how to learn to code
- **[Angular](https://gitter.im/angular/angular):** JavaScript framework to build applications
- **[Cypress](https://gitter.im/cypress-io/cypress):** Frontend testing framework
- **[Microsoft](https://gitter.im/Microsoft/home):** TypeScript, VSCode, and many more
- **[Scala](https://gitter.im/scala/scala):** Programming language
- [List of Gitter's most active communities](https://gitlab.com/gitlab-org/gitter/webapp/issues/2356#note_249999842)

## Top user issue(s)

- [Message reactions](https://gitlab.com/groups/gitlab-org/-/epics/396)
- [Link GitLab account to existing Gitter account](https://gitlab.com/gitlab-org/gitter/webapp/issues/1752)
  - This entails supporting multiple backing identities for a single user and [setting your own username](https://gitlab.com/gitlab-org/gitter/webapp/issues/1851) which is another top issue
- [Better notification settings and emails](https://gitlab.com/gitlab-org/gitter/webapp/issues/1205)

## Top internal customer issue(s)

The GitLab team does not use Gitter for internal communication.

There is a [GitLab community](https://gitter.im/gitlabhq/home) on Gitter with rooms for [general GitLab support](https://gitter.im/gitlabhq/gitlabhq) and where [GitLab contributors can chat](https://gitter.im/gitlabhq/contributors).

## Top Vision Item(s)

- [Message reactions](https://gitlab.com/groups/gitlab-org/-/epics/396)
- [Multiple identity users](https://gitlab.com/groups/gitlab-org/-/epics/400)
- [Better search](https://gitlab.com/groups/gitlab-org/-/epics/387)
- [Gitter x GitLab cross-references](https://gitlab.com/groups/gitlab-org/-/epics/402)
- [Post-creation community and room customization](https://gitlab.com/groups/gitlab-org/-/epics/399)
- [Progressive web app(PWA) as iOS/Android app alternative](https://gitlab.com/groups/gitlab-org/-/epics/386)
