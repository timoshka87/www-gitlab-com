---
layout: markdown_page
title: "Category Direction - ChatOps"
---

- TOC
{:toc}

## Introduction and how you can help

Thanks for visiting this category page on ChatOps in GitLab.

This vision is a work in progress and everyone can contribute. Sharing your feedback directly on our [chatops issues](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=chatops) and our public [epic](https://gitlab.com/groups/gitlab-org/-/epics/591) at GitLab.com is the best way to contribute to our vision.

## Overview

ChatOps, at its core, is a simple model of collaboration based on conversation. Conversation is the default and natural human way of working together and is still one of the most effective ways to collaborate despite changes to the medium of work conversations. In chat tools, such as [Slack](https://slack.com/), by bringing people, bots, and related tools all into the location where conversation occurs, enhanced efficiency, transparency, and learning is made possible.

It is useful to enable operators to work with GitLab in all of the chat mediums they use in their work place. GitLab should integrate with popular tools so that ChatOps works for GitLab users out of the box.

## What's next & why

[GitLab ChatOps](https://docs.gitlab.com/ee/ci/chatops/) currently supports Slack and GitLab CI/CD. Additional ChatOps capability will be introduced along with the maturation of other GitLab categories, such as [Incident Management](../../monitor/debugging_and_health/incident_management/)

We eventually plan to support integration with tools such as [Mattermost](https://mattermost.com/) or [Microsoft Teams](https://www.microsoft.com/en-us/microsoft-365/microsoft-teams/group-chat-software). Currently, we are not actively working on additional integrations. We would like the communities help to [introduce a pipeline type for chat-ops generated pipelines](https://gitlab.com/gitlab-org/gitlab/-/issues/26975). 

## Analyst landscape

TBD

## Top Customer Success/Sales issue(s)

TBD

## Top user issue(s)

[Slack Custom Actions for Messages](https://gitlab.com/gitlab-org/gitlab-ee/issues/6154)

## Top internal customer issue(s)

TBD

## Top Vision Item(s)

TBD
